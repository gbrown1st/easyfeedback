<?php
require_once 'mandrill-api-php/src/Mandrill.php';

$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');
$send_date = date('Y-m-d');

$stmt = mysqli_prepare($mysqli,
          "SELECT
	email_recipients.RecipientID ,
	email_recipients.RecipientFirstName ,
	email_recipients.RecipientLastName ,
	email_recipients.RecipientGender ,
	email_recipients.PractitionerName ,
	email_recipients.RecipientEmail ,
	email_recipients.RecipientLastVisitDate ,
	campaign_recipients.campaign_id ,
	stores_toc.store_name ,
	brands_toc.brand_logo ,
	brands_toc.brand_email ,
	brands_toc.brand_name
FROM
	email_recipients
JOIN campaign_recipients ON email_recipients.RecipientID = campaign_recipients.recipient_id
JOIN campaigns_toc ON campaign_recipients.campaign_id = campaigns_toc.campaign_id
JOIN stores_toc ON campaigns_toc.store_id = stores_toc.store_id
JOIN brands_toc ON stores_toc.brand_id = brands_toc.brand_id
WHERE
	campaign_is_current = 'y'
AND email_recipients.RecipientWaitseen NOT IN (0,3,8)
AND email_recipients.RecipientEmail REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$'
AND date(
	campaign_recipients.created_date
) = date(NOW())
ORDER BY
	brands_toc.brand_name");

    mysqli_stmt_bind_result($stmt, $row->RecipientID, $row->RecipientFirstName, $row->RecipientLastName, $row->RecipientGender, $row->PractitionerName, $row->RecipientEmail, $row->RecipientLastVisitDate, $row->campaign_id, $row->store_name, $row->brand_logo, $row->brand_email, $row->brand_name);
    mysqli_stmt_execute($stmt);


$brand_array = array();
$recipient_array = array();

while (mysqli_stmt_fetch($stmt)) {
	
	$brand_array[] = $row->brand_name;
	
	$logo_array = explode('.', $row->brand_logo);
	
	$logo_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/email/logo/'.$logo_array[0].'.png';
	$store_name = $row->store_name;
	$store_name = str_replace('The ', '', $store_name);
	
	$gender = $row->RecipientGender;
			  
	if ($gender == 'F') {
		$header_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/email/header-woman.jpg';
	} else {
		$header_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/email/header-man.jpg';
	}
	
	$email = $row->RecipientEmail;
	$from_email = $row->brand_email;
	
	$date = date_create($row->RecipientLastVisitDate);
	$last_visit = date_format($date, 'g:ia \o\n l jS F Y');
	
	$surveyURL = 'https://easyfeedback.myhealth1st.com.au/survey/survey_start.php?c='.$row->campaign_id.'&r='.$row->RecipientID;
	
	$unsubscribeURL = 'https://easyfeedback.myhealth1st.com.au/unsubscribe.php?c='.$row->campaign_id.'&r='.$row->RecipientID;
	
	$recipient_array[] = array("logo_src"=>$logo_src,"store_name"=>$store_name,"header_src"=>$header_src,"header_src"=>$header_src,"email"=>$email,"from_email"=>$from_email,"last_visit"=>$last_visit,"RecipientFirstName"=>$row->RecipientFirstName,"RecipientLastName"=>$row->RecipientLastName,"practitioner_name"=>$row->PractitionerName,"surveyURL"=>$surveyURL,"unsubscribeURL"=>$unsubscribeURL,"brand_name"=>$row->brand_name,"campaign_id"=>$row->campaign_id,"campaign_tag"=>'campaign_'.$row->campaign_id); 
	
	
}

foreach ($recipient_array as $recipient) {
	
	$merge_vars_array = array();
	$to = array();
	$from_email = '';
	$store_name = '';
	$brand_name = '';
	$campaign_id = 0;
	
	$from_email = $recipient['from_email'];
	$store_name = $recipient['store_name'];
	$brand_name= $recipient['brand_name'];
	$campaign_id = $recipient['campaign_id'];
	$camapaign_tag = $recipient['campaign_tag'];
		
	$merge_vars_array[] = array(
                'rcpt' => $recipient['email'],
                'vars' => array(
                    array(
                        'name' => 'fname',
                        'content' => $recipient['RecipientFirstName']
                    ),
					 array(
                        'name' => 'lname',
                        'content' => $recipient['RecipientLastName']
                    ),
					array(
                        'name' => 'surveyURL',
                        'content' => $recipient['surveyURL']
                    ),
					array(
                        'name' => 'unsubscribeURL',
                        'content' => $recipient['unsubscribeURL']
                    ),
					array(
						'name' => 'logo_src',
						'content' => $recipient['logo_src']
					),
					array(
						'name' => 'header_src',
						'content' => $recipient['header_src']
					),
					array(
						'name' => 'store_name',
						'content' => $recipient['store_name']
					),
					array(
						'name' => 'last_visit',
						'content' => $recipient['last_visit']
					),
					array(
						'name' => 'practitioner_name',
						'content' => $recipient['practitioner_name']
					)
		
		
                )
            );
	$to[] = array(
                'email' => $recipient['email'],
                'name' => $recipient['RecipientFirstName'].' '.$recipient['RecipientLastName'],
                'type' => 'to'
            );
	
		try {
			$mandrill = new Mandrill('g-WNr3dvxNEpsC5dkKCoVA');
			$template_name = 'easyfeedback';
			$template_content = [];
			$message = array(
				'subject' => 'We need your opinion',
				'from_email' => $from_email,
				'from_name' => $brand_name.' Feedback',
				'to' => $to,
				'headers' => array('Reply-To' => $from_email),
				'important' => false,
				'track_opens' => null,
				'track_clicks' => null,
				'auto_text' => null,
				'auto_html' => null,
				'inline_css' => null,
				'url_strip_qs' => null,
				'preserve_recipients' => null,
				'view_content_link' => null,
				'bcc_address' => 'gbrown@1stgrp.com.au',
				'tracking_domain' => null,
				'signing_domain' => null,
				'return_path_domain' => null,
				'merge' => true,
				'merge_language' => 'handlebars',
				'merge_vars' => $merge_vars_array,
				'tags' => array($camapaign_tag)
			);

			$async = true;
			$ip_pool = 'Main Pool';
			$send_at = $send_date;
			$result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);

		} catch(Mandrill_Error $e) {
			// Mandrill errors are thrown as exceptions
			echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
			// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
			throw $e;
		}
	
	}

mysqli_stmt_free_result($stmt);
mysqli_close($stmt);
mysqli_close($mysqli);



?>