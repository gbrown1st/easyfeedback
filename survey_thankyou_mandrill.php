<?php
require_once 'mandrill-api-php/src/Mandrill.php';

$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}


$stmt = mysqli_prepare($mysqli,
          "SELECT
email_recipients.RecipientID,
email_recipients.RecipientFirstName,
email_recipients.RecipientLastName,
email_recipients.RecipientEmail
FROM
	email_recipients
WHERE
	RecipientID > 505");

    mysqli_stmt_execute($stmt);
    mysqli_stmt_bind_result($stmt, $row->RecipientID, $row->RecipientFirstName, $row->RecipientLastName, $row->RecipientEmail);

$campaign_id = mt_rand(1,5);
switch ($campaign_id) {
	case 1:
		$logo_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/logo/kevinpaisley.svg';
		$store_name = 'Kevin Paisley Fashion Eyewear Sydney';
		break;
	case 2:
		$logo_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/logo/prevue.svg';
		$store_name = 'Prevue Eyewear Adelaide';
		break;
	case 3:
		$logo_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/logo/stacey.svg';
		$store_name = 'Stacey & Stacey Optometrists Hobart';
		break;
	case 4:
		$logo_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/logo/nib.svg';
		$store_name = 'nib Eyecare Centre Brisbane';
		break;
	case 5:
		$logo_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/logo/theopticalco.svg';
		$store_name = 'The Optical Co Melbourne';
		break;
}


$merge_vars_array = array();
$to = array();

while (mysqli_stmt_fetch($stmt)) {
	$merge_vars_array[] = array(
                'rcpt' => $row->RecipientEmail,
                'vars' => array(
                    array(
                        'name' => 'fname',
                        'content' => $row->RecipientFirstName
                    ),
					 array(
                        'name' => 'lname',
                        'content' => $row->RecipientLastName
                    ),
					array(
                        'name' => 'surveyURL',
                        'content' => 'https://easyfeedback.myhealth1st.com.au/survey/survey_start.php?c='.$campaign_id.'&r='.$row->RecipientID
                    )
                )
            );
	$to[] = array(
                'email' => $row->RecipientEmail,
                'name' => $row->RecipientFirstName.' '.$row->RecipientLastName,
                'type' => 'to'
            );
}

try {
    $mandrill = new Mandrill('g-WNr3dvxNEpsC5dkKCoVA');
    $template_name = 'easyfeedback';
    $template_content = [];
    $message = array(
        'subject' => 'easyFEEDBACK Mandrill Template API Test',
        'from_email' => 'gbrown@myhealth1st.com.au',
        'from_name' => 'Graham Brown',
        'to' => $to,
        'headers' => array('Reply-To' => 'gbrown@myhealth1st.com.au'),
        'important' => false,
        'track_opens' => null,
        'track_clicks' => null,
        'auto_text' => null,
        'auto_html' => null,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => null,
        'view_content_link' => null,
        'tracking_domain' => null,
        'signing_domain' => null,
        'return_path_domain' => null,
        'merge' => true,
        'merge_language' => 'handlebars',
        'global_merge_vars' => array(
            array(
                'name' => 'logo_src',
                'content' => $logo_src
            ),
			array(
                'name' => 'store_name',
                'content' => $store_name
            )
        ),
        'merge_vars' => $merge_vars_array 
    );
    $async = true;
    $ip_pool = 'Main Pool';
    $send_at = '2017-02-06';
    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
    print_r($result);
    /*
    Array
    (
        [0] => Array
            (
                [email] => recipient.email@example.com
                [status] => sent
                [reject_reason] => hard-bounce
                [_id] => abc123abc123abc123abc123abc123
            )
    
    )
    */
} catch(Mandrill_Error $e) {
    // Mandrill errors are thrown as exceptions
    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    throw $e;
}
?>