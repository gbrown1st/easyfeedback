<?php
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}


date_default_timezone_set('Australia/Melbourne');
$data_appt_data_date = date('Y-m-d H:i:s');

$json_stmt = mysqli_prepare($mysqli, "INSERT INTO daily_appt_data (data_appt_pubnub_channel, daily_appt_data, data_appt_data_date) VALUES (?, ?, ?)");
mysqli_stmt_bind_param($json_stmt, 'sss', $_POST['pubnubChannel'], $_POST['JSONData'], $data_appt_data_date);
mysqli_stmt_execute($json_stmt);

$campaign_stmt = mysqli_prepare($mysqli, "SELECT
	campaigns_toc.campaign_id ,
	stores_toc.store_code
FROM
	campaigns_toc
JOIN stores_toc ON campaigns_toc.store_id = stores_toc.store_id
WHERE
	campaign_is_current = 'y'
AND store_code IS NOT NULL");
	
      mysqli_stmt_execute($campaign_stmt);

      mysqli_stmt_bind_result($campaign_stmt, $row->campaign_id, $row->store_code);

$campaigns_array = array();

while (mysqli_stmt_fetch($campaign_stmt)) {

	$campaigns_array[] = array("campaign_id"=>$row->campaign_id, "store_code"=>$row->store_code);
		
}


$json = json_decode($_POST['JSONData']);

for ($i=0;$i<count($json);$i++) {
	
	$PMSPatientID = $json[$i]->PMSPatientID;
	$PMSUserIdentifier = $json[$i]->PMSUserIdentifier;
	$PractitionerName = $json[$i]->PractitionerName;
	$RecipientFirstName = $json[$i]->RecipientFirstName;
	$RecipientLastName = $json[$i]->RecipientLastName;
	$RecipientEmail = $json[$i]->RecipientEmail;
	$RecipientGender = $json[$i]->RecipientGender;
	$RecipientDOB = $json[$i]->RecipientDOB;
	$dob_array = explode('T', $RecipientDOB);
	$RecipientDOB = $dob_array[0];
	$RecipientState = $json[$i]->RecipientState;
	$RecipientPostcode = $json[$i]->RecipientPostcode;
	$BranchIdentifier = $json[$i]->BranchIdentifier;
	$StoreName = $json[$i]->StoreName;
	$StoreAddress = $json[$i]->StoreAddress;
	$StoreSuburb = $json[$i]->StoreSuburb;
	$StoreState = $json[$i]->StoreState;
	$StorePostcode = $json[$i]->StorePostcode;
	$RecipientHealthFund = $json[$i]->RecipientHealthFund;
	if (is_null($RecipientHealthFund)) {
		$RecipientHealthFund = '';
	}
	$RecipientLastVisitDate = $json[$i]->RecipientLastVisitDate;
	$RecipientWaitseen = $json[$i]->RecipientWaitseen;
	
	$stmt = mysqli_prepare($mysqli, "INSERT INTO email_recipients (PMSPatientID, PMSUserIdentifier, PractitionerName, RecipientFirstName, RecipientLastName, RecipientEmail, RecipientGender, RecipientDOB, RecipientState, RecipientPostcode, BranchIdentifier, StoreName, StoreAddress, StoreSuburb, StoreState, StorePostcode, RecipientHealthFund, RecipientLastVisitDate, RecipientWaitseen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	mysqli_stmt_bind_param($stmt, 'isssssssssssssssssi', $PMSPatientID, $PMSUserIdentifier, $PractitionerName, $RecipientFirstName, $RecipientLastName, $RecipientEmail, $RecipientGender, $RecipientDOB, $RecipientState, $RecipientPostcode, $BranchIdentifier, $StoreName, $StoreAddress, $StoreSuburb, $StoreState, $StorePostcode, $RecipientHealthFund, $RecipientLastVisitDate, $RecipientWaitseen);
	mysqli_stmt_execute($stmt);
	
	$key = array_search($BranchIdentifier, array_column($campaigns_array, 'store_code'));
	
	$campaign_id = $campaigns_array[$key]['campaign_id'];
	
	$recipient_id = mysqli_insert_id($mysqli);
	
	$cr_stmt = mysqli_prepare($mysqli, "INSERT INTO campaign_recipients (campaign_id, recipient_id) VALUES (?,?)");
	mysqli_stmt_bind_param($cr_stmt, 'ii', $campaign_id, $recipient_id);
	mysqli_stmt_execute($cr_stmt);

}

	  
 	  mysqli_stmt_free_result($json_stmt);
	  mysqli_close($json_stmt);
	  mysqli_stmt_free_result($stmt);
	  mysqli_close($stmt);
	  mysqli_stmt_free_result($cr_stmt);
	  mysqli_close($cr_stmt);
	  mysqli_close($mysqli);
?>
