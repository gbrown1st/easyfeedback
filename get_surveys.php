<?php
$mysqli = new mysqli('localhost:3306', 'gbrown', 'TuscanGuitars', 'easyRecalls');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_SESSION)) {
	session_start();
}

$surgery_id = $_SESSION['loginSurgeryID'];

$stmt = mysqli_prepare($mysqli,
          "SELECT
	surveys.survey_id ,
	surveys.survey_title ,
	surveys.survey_status
FROM
	surveys
WHERE
	surveys.surgery_id = ?
ORDER BY
	surveys.survey_title ASC");

	mysqli_stmt_bind_param($stmt, 'i', $surgery_id);
    mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->survey_id, $row->survey_title, $row->survey_status);

	  $response = '';


while (mysqli_stmt_fetch($stmt)) {
		// $response .= '<a id="s'.$row->survey_id.'" data-survey-status="'.$row->survey_status.'" onClick="selectSurvey('.$row->survey_id.')" href="#" class="list-group-item">'.$row->survey_title.'</a>';
    $response .= '<div class="row">
      <div class="col-xs-8 col-sm-9 col-md-10"><a id="s'.$row->survey_id.'" data-survey-status="'.$row->survey_status.'" onClick="selectSurvey('.$row->survey_id.')" href="#" class="list-group-item">'.$row->survey_title.'</a></div>
      <div class="col-xs-4 col-sm-3 col-md-2">
          <div class="input-group pull-right">
            <div id="'.$row->survey_id.'" class="btn-group">
                <span id="surveyStatusActive_'.$row->survey_id.'" class="btn btn-success btn-sm active" onClick="toggle('.$row->survey_id.', \'Active\')">Active</span>
                <span id="surveyStatusInactive_'.$row->survey_id.'" class="btn btn-default btn-sm notActive" onClick="toggle('.$row->survey_id.', \'Inactive\')">Inactive</span>
            </div>
            <input type="hidden" value="'.$row->survey_status.'" class="surveyStatusValue" name="surveyStatusValue" id="surveyStatusValue_'.$row->survey_id.'">
          </div>
        </div>
      </div>';
	}


/*

$response = ' <select id="surveys" name="surveys" class="selectpicker" ><option value="0" survey_status="">Select a survey</option>';

	while (mysqli_stmt_fetch($stmt)) {
		$response .= '<option value="'.$row->survey_id.'" survey_status="'.$row->survey_status.'">'.$row->survey_title.'</option>';

	}

	$response .= '</select>';
*/



	  mysqli_stmt_free_result($stmt);
	  //mysqli_close($stmt);
	  //mysqli_close($mysqli);

echo $response;

?>
