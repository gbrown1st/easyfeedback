<?php
require_once('lib/autoloader.php');
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

use Pubnub\Pubnub;

$pubnub = new Pubnub('pub-c-5a3f26dc-b33f-47a4-bac4-1f49d94f0ea6','sub-c-3001d7ac-0cf5-11e7-930d-02ee2ddab7fe');

$reset_stmt = mysqli_prepare($mysqli, "TRUNCATE heartbeats");
mysqli_stmt_execute($reset_stmt);

$stmt = mysqli_prepare($mysqli,
          "SELECT
	sms_practices.sms_practice_id
FROM
	sms_practices
WHERE
	sms_practices.sms_practice_active = 'y'");

    mysqli_stmt_bind_result($stmt, $row->sms_practice_id);
    mysqli_stmt_execute($stmt);


while (mysqli_stmt_fetch($stmt)) {
	$practiceID = $row->sms_practice_id;
	$pubnubChannel = 'SDBPSMS'.$practiceID;
	$publish_result = $pubnub->publish($pubnubChannel,array("Call"=>"PubNubHeartBeatBP","practiceID"=>$practiceID,"timeInterval"=>"5"));
}

mysqli_stmt_free_result($reset_stmt);
mysqli_stmt_free_result($stmt);
mysqli_close($stmt);
mysqli_close($mysqli);

