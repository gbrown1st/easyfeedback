<?php
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');
include'APIClient2.php';

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

date_default_timezone_set('Australia/Sydney');
$data_appt_data_date = date('Y-m-d H:i:s');
$json_stmt = mysqli_prepare($mysqli, "INSERT INTO daily_appt_data (data_appt_practice_id, daily_appt_data, data_appt_data_date) VALUES (?, ?, ?)");
mysqli_stmt_bind_param($json_stmt, 'iss', $_POST['practiceID'], $_POST['JSONData'], $data_appt_data_date);
mysqli_stmt_execute($json_stmt);

$json = json_decode($_POST['JSONData']);

//$api=new transmitsmsAPI("57f83f8a3504bc088f13c3e96821bfaa","kaWOVntcTG3gOy2uIGQbRTrVpCL65ZJj");
$api=new transmitsmsAPI("57f83f8a3504bc088f13c3e96821bfaa","kaWOVntcTG3gOy2uIGQbRTrVpCL65ZJj");

for ($i=0;$i<count($json);$i++) {
	
	$PATIENTID = $json[$i]->PATIENTID;
	$FIRSTNAME = $json[$i]->FIRSTNAME;
	$SURNAME = $json[$i]->SURNAME;
	$MOBILEPHONE = $json[$i]->MOBILEPHONE;
	$CONSENTSMSREMINDER = (int) $json[$i]->CONSENTSMSREMINDER;
	$APPOINTMENTDATE = $json[$i]->APPOINTMENTDATE;
	$APPOINTMENTTIME = (int) $json[$i]->APPOINTMENTTIME;
	$APPOINTMENTTIME = strtotime($APPOINTMENTDATE) + $APPOINTMENTTIME;
	$APPOINTMENTTIMESTRING = date('D F j Y \a\t g:i a', $APPOINTMENTTIME);
	$PRACTITIONER = $json[$i]->PRACTITIONER;
	$PRACTICENAME = $json[$i]->PRACTICENAME;
	$MOBILEPHONE = preg_replace('/\D/', '', $MOBILEPHONE);
	if (! preg_match('/^0\d{9}$/', $MOBILEPHONE)) {
		$valid_mobile = 'n';
	} else {
		$mobile_result=$api->formatNumber($MOBILEPHONE, 'AU');
		if($mobile_result->error->code=='SUCCESS')
		 {
			 $MOBILEPHONE = $mobile_result->number->international;
			 $valid_mobile = 'y';
		 } else {
			 $valid_mobile = 'n';
		 }
	}
	$stmt = mysqli_prepare($mysqli, "REPLACE INTO sms_recipients (sms_recipient_pms_patient_id, sms_recipient_firstname, sms_recipient_surname, sms_recipient_mobilephone, sms_recipient_valid_mobile, sms_recipient_appointmentdate, sms_recipient_appointmenttime, sms_recipient_practitioner, sms_recipient_practice_id, sms_recipient_practicename) VALUES (?,?,?,?,?,?,?,?,?,?)");
	mysqli_stmt_bind_param($stmt, 'isssssssis', $PATIENTID, $FIRSTNAME, $SURNAME, $MOBILEPHONE, $valid_mobile, $APPOINTMENTDATE, $APPOINTMENTTIMESTRING, $PRACTITIONER, $_POST['practiceID'], $PRACTICENAME);
	mysqli_stmt_execute($stmt);
	
	$practice_id = $_POST['practiceID'];
	
	$practice_stmt = mysqli_prepare($mysqli,
          "SELECT
	sms_practices.sms_practice_message_template ,
	sms_practices.sms_obey_consent
FROM
	sms_practices
WHERE
	sms_practices.sms_practice_active = 'y'
AND sms_practices.sms_practice_id = $practice_id");

    mysqli_stmt_bind_result($practice_stmt, $row->sms_practice_message_template, $row->sms_obey_consent);
    mysqli_stmt_execute($practice_stmt);

while (mysqli_stmt_fetch($practice_stmt)) {
	$message_template = $row->sms_practice_message_template;
	$sms_obey_consent =  $row->sms_obey_consent;
}
	$message_template = str_replace('{firstname}', $FIRSTNAME, $message_template);
	$message_template = str_replace('{surname}', $SURNAME, $message_template);
	$message_template = str_replace('{practice}', $PRACTICENAME, $message_template);
	$message_template = str_replace('{practitioner}', $PRACTITIONER, $message_template);
	$message_template = str_replace('{appttime}', $APPOINTMENTTIMESTRING, $message_template);
	
	if ($valid_mobile == 'y' && ($sms_obey_consent == 'n' || $sms_obey_consent == 'y' && $CONSENTSMSREMINDER == 1)) {
		$result=$api->sendSms($message_template,"$MOBILEPHONE,61410183669,61450155277,61448992599");
	} 
	
	if($result->error->code=='SUCCESS')
	 {
		 $message_stmt = mysqli_prepare($mysqli, "INSERT INTO sms_message_sends (sms_recipient_practice_id, sms_recipient_pms_patient_id, sms_message, sms_message_status, sms_message_result, sms_message_id, sms_send_cost, sms_send_time) VALUES (?,?,?,?,?,?,?,?)");
		mysqli_stmt_bind_param($message_stmt, 'iisssids', $_POST['practiceID'], $PATIENTID, $message_template, $result->error->code, print_r($result), $result->message_id, $result->cost, $data_appt_data_date);
		mysqli_stmt_execute($message_stmt);
	 }
	 else
	 {
		 $message_stmt = mysqli_prepare($mysqli, "INSERT INTO sms_message_sends (sms_recipient_practice_id, sms_recipient_pms_patient_id, sms_message, sms_message_status, sms_message_result, sms_send_time) VALUES (?,?,?,?,?,?)");
		mysqli_stmt_bind_param($message_stmt, 'iissss', $_POST['practiceID'], $PATIENTID, $message_template, $result->error->code, print_r($result), $data_appt_data_date);
		mysqli_stmt_execute($message_stmt);
	 }
	
}
mysqli_stmt_free_result($stmt);
mysqli_stmt_free_result($practice_stmt);
mysqli_stmt_free_result($message_stmt);
mysqli_stmt_free_result($json_stmt);
mysqli_close($stmt);
mysqli_close($practice_stmt);
mysqli_close($message_stmt);
mysqli_close($json_stmt);
mysqli_close($mysqli);

?>
