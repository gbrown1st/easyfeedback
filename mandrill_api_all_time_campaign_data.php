<?php
require_once 'mandrill-api-php/src/Mandrill.php';

$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

$campaigns_array = array();

	try {
		$mandrill = new Mandrill('g-WNr3dvxNEpsC5dkKCoVA');
		$result = $mandrill->tags->allTimeSeries();
		
		
		
		
		for ($i=0;$i<count($result);$i++) {
			
			$tag = $result[$i]['tag'];
			$campaign_array = explode('_', $tag);
			
			if ($campaign_array[0] == 'campaign') {
			
				$campaign_id = $campaign_array[1];
				$json_stmt = mysqli_prepare($mysqli, "REPLACE INTO mandrill_api_campaign_data_by_day 
		(mandrill_api_campaign_data_by_day.campaign_id,
		mandrill_api_campaign_data_by_day.mandrill_data_sent,
		mandrill_api_campaign_data_by_day.mandrill_data_hard_bounces,
		mandrill_api_campaign_data_by_day.mandrill_data_soft_bounces,
		mandrill_api_campaign_data_by_day.mandrill_data_rejects,
		mandrill_api_campaign_data_by_day.mandrill_data_complaints,
		mandrill_api_campaign_data_by_day.mandrill_data_unsubs,
		mandrill_api_campaign_data_by_day.mandrill_data_opens,
		mandrill_api_campaign_data_by_day.mandrill_data_clicks,
		mandrill_api_campaign_data_by_day.mandrill_data_unique_opens,
		mandrill_api_campaign_data_by_day.mandrill_data_unique_clicks,
		mandrill_api_campaign_data_by_day.mandrill_data_time,
		mandrill_api_campaign_data_by_day.mandrill_data_tag,
		mandrill_api_campaign_data_by_day.mandrill_data_ef_record_created_time) 
		VALUES 
		(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
				mysqli_stmt_bind_param($json_stmt, 'iiiiiiiiiiisss', $campaign_id, $result[$i]['sent'], $result[$i]['hard_bounces'], $result[$i]['soft_bounces'], $result[$i]['rejects'], $result[$i]['complaints'], $result[$i]['unsubs'], $result[$i]['opens'], $result[$i]['clicks'], $result[$i]['unique_opens'], $result[$i]['unique_clicks'], $result[$i]['time'], $result[$i]['tag'], $now_date_time);
				mysqli_stmt_execute($json_stmt);
			}
		}
    	
	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		throw $e;
	}



mysqli_stmt_free_result($stmt);
mysqli_close($stmt);
mysqli_stmt_free_result($json_stmt);
mysqli_close($json_stmt);
mysqli_close($mysqli);



?>