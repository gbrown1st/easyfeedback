<?php
require_once 'mandrill-api-php/src/Mandrill.php';

$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');
$unsubscibe_id = $_GET['r'];
$unsubscibe_campaign = $_GET['c'];

$stmt = mysqli_prepare($mysqli,
          "SELECT
	email_recipients.RecipientEmail ,
	email_recipients.RecipientGender ,
	brands_toc.brand_name ,
	brands_toc.brand_logo
FROM
	email_recipients
JOIN campaign_recipients ON email_recipients.RecipientID = campaign_recipients.recipient_id
JOIN campaigns_toc ON campaign_recipients.campaign_id = campaigns_toc.campaign_id
JOIN stores_toc ON campaigns_toc.store_id = stores_toc.store_id
JOIN brands_toc ON stores_toc.brand_id = brands_toc.brand_id
WHERE
	RecipientID = $unsubscibe_id
AND campaigns_toc.campaign_id = $unsubscibe_campaign");

    mysqli_stmt_bind_result($stmt, $row->RecipientEmail, $row->RecipientGender, $row->brand_name, $row->brand_logo);
    mysqli_stmt_execute($stmt);

$unsubscribe_email = '';
$unsubscribe_brand = '';
$unsubscribe_logo = '';

while (mysqli_stmt_fetch($stmt)) {
	$unsubscribe_email = $row->RecipientEmail;
	$unsubscribe_gender = $row->RecipientGender;
	if ($unsubscribe_gender == 'F') {
		$header_src = 'survey/images/email/header-woman.jpg';
	} else {
		$header_src = 'survey/images/email/header-man.jpg';
	}
	$unsubscribe_brand = $row->brand_name;
	$logo_array = explode('.', $row->brand_logo);
	$unsubscribe_logo = 'survey/images/email/logo/'.$logo_array[0].'.png';
}

try {
    $mandrill = new Mandrill('g-WNr3dvxNEpsC5dkKCoVA');
    $email = $unsubscribe_email;
    $comment = 'Unsubscribed from '.$unsubscribe_brand. ' - '.$now_date_time;
    $result = $mandrill->rejects->add($email, $comment);
    //print_r($result);
    /*
    Array
    (
        [email] => example email
        [added] => 1
    )
    */
} catch(Mandrill_Error $e) {
    // Mandrill errors are thrown as exceptions
    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    throw $e;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
  <style type="text/css">
}	@media only screen and (max-width: 600px){
		.logo-image {
			width:300px !important;
			height:auto !important;
			}
		}
	@media only screen and (max-width: 600px){
		.hero-image {
			width:100% !important;
			height:auto !important;
			}
		}
	@media only screen and (max-width: 600px){
		.template-label {
			width:100% !important;
			line-height:50% !important;
			}
		}

}	@media only screen and (max-width: 600px){
		table[class=container] .block-grid{
			max-width:9999px !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=container] .block-grid td{
			padding-left:0;
			padding-right:0;
			width:100% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .heading{
			font-size:28px;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] center{
			min-width:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .container{
			width:95% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .row{
			width:100% !important;
			display:block !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .wrapper{
			display:block !important;
			padding-right:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns{
			table-layout:fixed !important;
			float:none !important;
			width:100% !important;
			padding-right:0px !important;
			padding-left:0px !important;
			display:block !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column{
			table-layout:fixed !important;
			float:none !important;
			width:100% !important;
			padding-right:0px !important;
			padding-left:0px !important;
			display:block !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .wrapper.first .columns{
			display:table !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .wrapper.first .column{
			display:table !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] table.columns td{
			width:100% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] table.column td{
			width:100% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.one{
			width:8.333333% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.one{
			width:8.333333% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.two{
			width:16.666666% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.two{
			width:16.666666% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.three{
			width:25% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.three{
			width:25% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.four{
			width:33.333333% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.four{
			width:33.333333% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.five{
			width:41.666666% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.five{
			width:41.666666% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.six{
			width:70% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.six{
			width:50% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.seven{
			width:58.333333% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.seven{
			width:58.333333% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.eight{
			width:66.666666% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.eight{
			width:66.666666% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.nine{
			width:75% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.nine{
			width:75% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.ten{
			width:83.333333% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.ten{
			width:83.333333% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.eleven{
			width:91.666666% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.eleven{
			width:91.666666% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .columns td.twelve{
			width:100% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .column td.twelve{
			width:100% !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-one{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-two{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-three{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-four{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-five{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-six{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-seven{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-eight{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-nine{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-ten{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] td.offset-by-eleven{
			padding-left:0 !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] table.columns td.expander{
			width:1px !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .right-text-pad{
			padding-left:20px !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .text-pad-right{
			padding-left:20px !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .left-text-pad{
			padding-right:20px !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .text-pad-left{
			padding-right:20px !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .hide-for-small{
			display:none !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .show-for-desktop{
			display:none !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .show-for-small{
			display:inherit !important;
		}

}	@media only screen and (max-width: 600px){
		table[class=body] .hide-for-desktop{
			display:inherit !important;
		}

}</style></head>
  <body style="width: 100% !important; min-width: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; background: #eeeeee; margin: 0; padding: 0;" bgcolor="#eeeeee">
	<table class="body" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; height: 100%; width: 100%; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; background: #eeeeee; margin: 0; padding: 0;" bgcolor="#eeeeee"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="center" align="center" valign="top" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;">
        <center style="width: 100%; min-width: 580px;">
          <table class="row header" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; background: #ffffff; padding: 0px;" bgcolor="#eeeeee"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="center" align="center" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" valign="top">
                <center style="width: 100%; min-width: 580px;">

                  <table class="container" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 580px; background: #fff; margin: 0 auto; padding: 0;" bgcolor="transparent"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top">
                        <table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="six sub-columns" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; min-width: 0px; width: 200;" align="left" valign="top">
                              <img src="<?php echo $unsubscribe_logo; ?>" alt="" width="100%" class="logo-image" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; float: left; clear: both; display: block;" align="left">
                            </td>
                            <td class="six sub-columns last" align="right" style="text-align: right; vertical-align: middle; word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; min-width: 0px; width: 50%; color: #666666; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0px 0px 10px;" valign="middle">
                              <span class="template-label" style="color: #999999; font-weight: normal; font-size: 12px;">&nbsp;</span>
                            </td>
                            <td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" align="left" valign="top"></td>
                          </tr></table></td>
                    </tr></table></center>
              </td>
            </tr></table><table class="row hero" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; background: #eeeeee; padding: 0px;" bgcolor="#eeeeee"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="center" align="center" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" valign="top"> <!-- Center the container -->
					<center style="width: 100%; min-width: 580px;">
						<!-- content start -->
						<table class="container" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 580px; background: #eeeeee; margin: 0 auto; padding: 0;" bgcolor="#eeeeee"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="hero wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0 0px 0px;" align="left" valign="top">
									<table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #666666; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0px 0px 0;" align="left" valign="top">
												<img src="<?php echo $header_src; ?>" alt="" border="0" mc:edit="banner_image" align="center" class="hero-image" width="580" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; max-width: 100%; float: left; clear: both; display: block; margin: 0; padding: 0;"></td>
											<td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" align="left" valign="top"></td>
										</tr></table></td>
							</tr></table></center>
				</td>
			</tr></table><table class="container" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: inherit; width: 580px; background: #ffffff; margin: 0 auto; padding: 0;" bgcolor="#ffffff"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" align="left" valign="top">
					<!-- content start -->
					<table class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top">
								<table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="text-pad" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0px 20px 10px;" align="left" valign="top">

											<h2 mc:edit="heading_text" class="heading center" style="text-align: center; border-bottom-style: solid; border-bottom-color: #eeeeee; border-bottom-width: 1px; color: #808080; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 1.3; overflow-wrap: normal !important; word-break: normal !important; word-wrap: normal !important; hyphens: none !important; -webkit-hyphens: none !important; font-size: 32px; margin: 0; padding: 20px 25px 25px;" align="center">You've been unsubscribed</h2>
										</td>
										<td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" align="left" valign="top"></td>
									</tr></table></td>
						</tr></table><table border="0" class="row" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top">
								<table border="0" class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td mc:edit="body" class="text-pad" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0px 40px 20px;" align="left" valign="top"><p>The email address <?php echo $unsubscribe_email; ?> has been removed from our feedback list.</p>
								  <p>Thank you for being a customer of <?php echo $unsubscribe_brand; ?>. We hope to see you again soon.</p></td>
										<td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" align="left" valign="top"></td>
									</tr></table></td>
						</tr></table>
								<table border="0" class="row" mc:hideable="" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top">
								<table border="0" class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="text-pad" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0px 20px 10px;" align="left" valign="top">

											<hr style="color: #d9d9d9; height: 1px; background: #d9d9d9; border: none;"></td>
										<td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" align="left" valign="top"></td>
									</tr></table></td>
						</tr></table>
<!-- 						<table border="5" class="row" mc:hideable style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="wrapper last" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; position: relative; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 10px 0px 0px;" align="left" valign="top"> -->

								<table class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 580px; margin: 0 auto; padding: 0;"><tr style="vertical-align: top; text-align: left; padding: 0;" align="left"><td class="text-pad center" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: center; color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0px 20px 10px;" align="center" valign="top">
											<p mc:edit="closing_text" style="color: #666666; font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 14px; overflow-wrap: normal !important; word-break: normal !important; word-wrap: normal !important; hyphens: none !important; -webkit-hyphens: none !important; margin: 0 0 25px; padding: 0;">As always, our support team is available on <a href="tel:61292124871" target="_blank" style="text-decoration: none;">+61&nbsp;2&nbsp;9212&nbsp;4871</a> or via <a href="mailto:info@theopticalco.com.au" target="_blank" style="text-decoration: none;">info@theopticalco.com.au</a> should you need any assistance.</p>
										</td>
<!--										<td class="expander" style="word-break: break-word; -webkit-hyphens: auto; -moz-hyphens: auto; hyphens: auto; border-collapse: collapse !important; vertical-align: top; text-align: left; visibility: hidden; width: 0px; color: #666666; font-family: 'Helvetica', 'Arial', sans-serif; font-weight: normal; line-height: 25px; font-size: 18px; margin: 0; padding: 0;" align="left" valign="top">
										</td>-->
<!--
									</tr>
								</table>

							</td>-->
						</tr>
						<tr>
							<td class="twelve columns" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 10px 0 10px 0; background: #f8f8f8;" bgcolor="#f8f8f8">
								
								<center style="width: 100%; min-width: 580px;">
								<a href="https://www.myhealth1st.com.au" target="_blank" style="color: #db1b4c; text-decoration: none;">
									<img src="https://easyfeedback.myhealth1st.com.au/survey/images/email/poweredbymh1.png" alt="Powered by My Health 1st" class="logo-image" height="60" width="250" style="outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; display: block;" align="center">
								</a>
								</center>
							</td>
						</tr>
						<tr>
							<td class="row footer" style="border-spacing: 0; border-collapse: collapse; vertical-align: top; text-align: left; width: 100%; position: relative; display: block; padding: 0px 0 20px 0; background: #eeeeee;" bgcolor="#eeeeee">
							
								<!-- footer content -->
								<center style="width: 100%; min-width: 580px;"><small style="font-size: 12px;"><br>Copyright &copy; 2017 The Optical Co Pty Ltd and related parties.<br><br></small></center>
								<!--/footer content-->

							
							</td>
						</tr>
					</table>
						
						<!-- container end below -->
			</td>
			</tr></table></center>
			</td>
		</tr></table></body>
</html>