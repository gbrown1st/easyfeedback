<?php
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}
$practice_clause = '';
if (isset($_GET['practiceID'])) {
	$practiceID = $_GET['practiceID'];
	$practice_clause = "WHERE sms_practices.sms_practice_id = $practiceID";
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	sms_practices.sms_practice_name,
	sms_recipients.sms_recipient_practitioner ,
	sms_recipients.sms_recipient_firstname ,
	sms_recipients.sms_recipient_surname ,
	sms_recipients.sms_recipient_mobilephone ,
	sms_recipients.sms_recipient_valid_mobile ,
	sms_message_sends.sms_message ,
	sms_message_sends.sms_message_status ,
	sms_message_sends.sms_send_time 
FROM
	sms_message_sends
JOIN sms_recipients ON sms_message_sends.sms_recipient_practice_id = sms_recipients.sms_recipient_practice_id
AND sms_message_sends.sms_recipient_pms_patient_id = sms_recipients.sms_recipient_pms_patient_id
JOIN sms_practices ON sms_recipients.sms_recipient_practice_id = sms_practices.sms_practice_id
$practice_clause
ORDER BY
sms_recipients.sms_recipient_practicename ASC,
sms_message_sends.sms_send_time ASC");

    mysqli_stmt_bind_result($stmt, $row->sms_practice_name, $row->sms_recipient_practitioner, $row->sms_recipient_firstname, $row->sms_recipient_surname, $row->sms_recipient_mobilephone, $row->sms_recipient_valid_mobile, $row->sms_message, $row->sms_message_status, $row->sms_send_time);

    mysqli_stmt_execute($stmt);

$report_array = array();

while (mysqli_stmt_fetch($stmt)) {
	
		$object = new stdClass();
	    $object->sms_practice_name = $row->sms_practice_name;
		$object->sms_recipient_practitioner = $row->sms_recipient_practitioner;
		$object->sms_recipient_firstname = $row->sms_recipient_firstname;
		$object->sms_recipient_surname = $row->sms_recipient_surname;
		$object->sms_recipient_mobilephone = $row->sms_recipient_mobilephone;
		if ($row->sms_recipient_valid_mobile == 'y') {
			$object->sms_recipient_valid_mobile = 'Yes';
		} else {
			$object->sms_recipient_valid_mobile = 'No';
		}
		$object->sms_message = $row->sms_message;
		$object->sms_message_status = $row->sms_message_status;
		$object->sms_send_time = $row->sms_send_time;
		$report_array[] = $object;
}

$html = '<table>
			  <tbody>
				 <tr>
				 	<th>Practice</th>
					<th>Practitioner</th>
					<th>Recipient First Name</th>
					<th>Recipient Last Name</th>
					<th>Recipient Mobile Number</th>
					<th>Recipient Mobile Number Is Valid</th>
					<th>SMS Message</th>
					<th>SMS Message Status</th>
					<th>SMS Message Send Time</th>
			      </tr>';

for ($i=0;$i<count($report_array);$i++) {
	
	$html .= '
		<tr>
					<td>'.$report_array[$i]->sms_practice_name.'</td>
					<td>'.$report_array[$i]->sms_recipient_practitioner.'</td>
					<td>'.$report_array[$i]->sms_recipient_firstname.'</td>
					<td>'.$report_array[$i]->sms_recipient_surname.'</td>
					<td>'.$report_array[$i]->sms_recipient_mobilephone.'</td>
					<td>'.$report_array[$i]->sms_recipient_valid_mobile.'</td>
					<td>'.$report_array[$i]->sms_message.'</td>
					<td>'.$report_array[$i]->sms_message_status.'</td>
					<td>'.$report_array[$i]->sms_send_time.'</td>
		</tr>';
	}
	
$html .= '
		</tbody>
	</table>';

echo $html;

mysqli_stmt_free_result($stmt);
mysqli_close($stmt);
mysqli_close($mysqli);



?>