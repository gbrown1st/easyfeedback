<?php require('_header.php'); ?>

	<main>
		<div class="content-header">
			<div class="fluid-container">
				<div class="heading">
					<h2>Create new survey</h2>
					<p>Add new survey</p>
				</div><!-- .heading -->

				<div class="middle-col">
					<h3 class="cancel-question"><a href="survey_list.php">Cancel new survey</a></h3>
				</div><!-- .middle-col -->

				<div class="user">
					<div class="photo" style="background-image: url(images/<?php echo($_SESSION['loginImageURL']); ?>)"></div>
					<div class="text">
						<div class="name"><?php echo($_SESSION['loginName']); ?></div>
						<div class="role"><?php echo($_SESSION['loginAdminRole']); ?></div>
					</div><!-- .text -->
				</div><!-- .user -->
			</div><!-- .container -->
		</div><!-- .content-header -->

		<div class="content-main">

			<form action="#" class="search_form">
				<input type="text" id="survey_search"/>
				<label for="survey_search">Search existing questions</label>
				<button class="btn btn-primary btn-search" type="submit"><span class="hidden">Search</span></button>
			</form>
			<p class="small">Does this survey already exist?</p>

			<div class="survey_table"> <!-- populated via survey_search_ajax.js --></div>
				
		</div><!-- .content-main -->

	</main>

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/search_form.js"></script>
	<script src="js/survey_search_ajax.js"></script>
	<script src="js/onoffswitches.js"></script>
	<script src="js/row_hints.js"></script>
	<script src="js/survey_row_links.js"></script>
</body>
</html>
