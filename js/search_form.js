$(function() {

	//search field
	var search_field = $('.search_form input');
	var search_button = $('.search_form button');
	var search_label = $('.search_form label');

	//show/hide label
	search_field.on('focus', function() {
		search_label.hide();
	});

	search_field.on('blur', function() {
		//don't show label if user entered text
		if( $(this).val() === '') {
			search_label.show();
		}
	});

	//replace search button with 'clear' icon
	search_field.on('keyup', function(e) { 
		if( $(this).val() === '') 
			search_button.removeClass('clear');
		else
			search_button.addClass('clear');
	});

	//clear search field
	$(document).on('click','.search_form button.clear', function(e){ 
		e.preventDefault();
		search_field.val('').focus();
		search_button.removeClass('clear');
	});

});
