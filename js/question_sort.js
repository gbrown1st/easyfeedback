$(function() {
	var rows = $('.questions_table .row');

	//see https://github.com/RubaXa/Sortable
	var sortable = Sortable.create( $('.questions_table')[0], {
		handle: '.handle',
		draggable: '.row',
		onStart: function(event) {
			rows.find('.number').removeClass('highlight');
			$(event.item).addClass('dragging').find('.number').addClass('highlight');
		},
		onEnd: function(event) {
			$(event.item).removeClass('dragging');
		},
		onUpdate: function(event) {
			var survey_id = $('.questions_table').data('survey-id');
			var questions = [];

			$(event.srcElement).find('.row').each( function(index) {
				$(this).find('.number span').text(index+1);
				questions.push( $(this).data('question-id') );
			});
			
			$.ajax({
					method: 'POST',
					url: '/easyfeedback/ajax_backend/save_survey_question_order.php',
					data: {
						survey_id: survey_id,
						question_order: questions
					},
					success: function(response) {
						//just to prove it's saving correctly
						console.log(response);
					}
				});
		}
	});
	

});
