$(function() {

	//stop the click from propagating to the container row, which has it's own click handler
	$(document).on('click', '.onoffswitch', function(e) {
		e.stopPropagation();
	});

	//survey table on/off switches
	$(document).on('change', '.onoffswitch input[type="checkbox"]', function() {
		var row = $(this).closest('.row');
		if( $(this).prop('checked') )
			row.removeClass('disabled');
		else
			row.addClass('disabled');
	});

});
