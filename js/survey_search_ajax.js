$(function() {

	var survey_search_field = $('#survey_search');
	var results_table = $('.survey_table');

	function do_search() {
		$.ajax({
			method: 'POST',
			url: './ajax_backend/survey_search.php',
			data: {search_string: survey_search_field.val()},
			success: function(response) {
				results_table.html(results_rows(response));
			}
		});
	}

	//get all results on page load
	do_search();

	//when the field changes, update the results
	survey_search_field.on('keyup', do_search);

	//format the result
	function results_rows(json_data) {
		var return_string = '';
		var i;
		for( i=0; i<json_data.length; i++ ) {
				
			var disabled = '';
			var checked = 'checked';
			if( json_data[i].active === false ) {
				disabled = ' disabled';
				checked = '';
			}

			return_string += '<div class="row'+ disabled +'" data-edit-href="survey_edit.php?id='+json_data[i].id+'">' +
					'<div class="icon-holder"><i class="icon icon-page"></i></div>' +
					'<div class="title">'+ json_data[i].name +'</div>' +
					'<div class="group">' + json_data[i].group + '</div><!-- .group -->' +
					'<div class="onoffswitch-holder">' +
						'<div class="onoffswitch">' +
							'<input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch_'+json_data[i].id+'" '+ checked +'>' +
							'<label class="onoffswitch-label" for="myonoffswitch_'+json_data[i].id+'">' +
								'<span class="onoffswitch-inner"></span>' +
								'<span class="onoffswitch-switch"></span>' +
							'</label>' +
						'</div><!-- .onoffswitch -->' +
					'</div><!-- .onoffswitch-holder -->' +
					'<div class="hint">Enable survey</div>' +
				'</div><!-- .row -->';
		}

		return return_string;
	}

});
