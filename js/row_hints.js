$(function() {
	//show hint on hover - styled checkboxes
	$(document).on('mouseenter', '.onoffswitch', function() { 
		$(this).closest('.row').find('.hint').addClass('visible');
	});

	$(document).on('mouseleave', '.onoffswitch', function() { 
		$(this).closest('.row').find('.hint').removeClass('visible');
	});

	//show hint on hover - question edit, delete
	$('.actions .edit, .actions .delete').on('mouseenter', function() {
		var hint_text = $(this).data('hint');
		$(this).closest('.row').find('.hint').text( hint_text ).addClass('visible');
	});

	$('.actions .edit, .actions .delete').on('mouseleave', function() {
		$(this).closest('.row').find('.hint').removeClass('visible');
	});
});
