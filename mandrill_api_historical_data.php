<?php
require_once 'mandrill-api-php/src/Mandrill.php';

$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');
$mandrill_from_date = date('Y-m-d', strtotime('-30 days'));
$mandrill_to_date = date('Y-m-d');

$stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	email_recipients.RecipientID ,
	email_recipients.RecipientEmail ,
	campaign_recipients.campaign_id 
FROM
	email_recipients
JOIN campaign_recipients ON email_recipients.RecipientID = campaign_recipients.recipient_id
JOIN campaigns_toc ON campaign_recipients.campaign_id = campaigns_toc.campaign_id
WHERE
	campaign_is_current = 'y'
AND email_recipients.RecipientEmail REGEXP '^[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$'
AND date(
	campaign_recipients.created_date
) >= '$mandrill_from_date'
AND date(
	campaign_recipients.created_date
) <= '$mandrill_to_date'
ORDER BY
	email_recipients.RecipientID");

    mysqli_stmt_bind_result($stmt, $row->RecipientID, $row->RecipientEmail, $row->campaign_id);
    mysqli_stmt_execute($stmt);

$recipient_array = array();
$mandrill_send_data_array = array();

while (mysqli_stmt_fetch($stmt)) {
	$recipient_array[] = array("RecipientFirstName"=>$row->RecipientFirstName,"RecipientEmail"=>$row->RecipientEmail,"campaign_id"=>$row->campaign_id); 
}

	try {
		
    $mandrill = new Mandrill('g-WNr3dvxNEpsC5dkKCoVA');
    $query = 'subject: We need your opinion';
    $date_from = $mandrill_from_date;
    $date_to = $mandrill_send_date;
    $tags = array();
    $senders = array();
    $api_keys = array('g-WNr3dvxNEpsC5dkKCoVA');
    $limit = 1000;
    $result = $mandrill->messages->search($query, $date_from, $date_to, $tags, $senders, $api_keys, $limit);
		
		
				for ($i=0;$i<count($result);$i++) {
					if ($result[$i]['state'] == 'sent') {
						
						$recipient_stmt = mysqli_prepare($mysqli,
          "SELECT DISTINCT
	email_recipients.RecipientID ,
	campaign_recipients.campaign_id 
FROM
	email_recipients
JOIN campaign_recipients ON email_recipients.RecipientID = campaign_recipients.recipient_id
JOIN campaigns_toc ON campaign_recipients.campaign_id = campaigns_toc.campaign_id
WHERE
	email_recipients.RecipientEmail = ?");

    mysqli_stmt_bind_result($recipient_stmt, $row->RecipientID, $row->campaign_id);
    mysqli_stmt_bind_param($recipient_stmt, 's', $result[$i]['email']);
	mysqli_stmt_execute($recipient_stmt);
					
				while (mysqli_stmt_fetch($recipient_stmt)) {
						
						$ef_recipient_id = $row->RecipientID;
						$ef_campaign_id = $row->campaign_id;
						$mandrill_message_id = $result[$i]['_id'];
						$mandrill_send_date = gmdate("Y-m-d", $result[$i]['ts']);
						$mandrill_send_tags = $result[$i]['tags'][0];
						$mandrill_send_opens = $result[$i]['opens'];
						$mandrill_send_clicks = $result[$i]['clicks'];
						$mandrill_send_json = json_encode($result[$i]);
						
						$mandrill_send_data_array[] = array("ef_recipient_id"=>$ef_recipient_id,"ef_campaign_id"=>$ef_campaign_id,"mandrill_message_id"=>$mandrill_message_id,"mandrill_send_date"=>$mandrill_send_date,"mandrill_send_data_tag"=>$mandrill_send_tags,"mandrill_send_data_opens"=>$mandrill_send_opens,"mandrill_send_data_clicks"=>$mandrill_send_clicks,"mandrill_send_data_json"=>$mandrill_send_json); 
						
				}
						
			}
				
		}
			
    	for ($d=0;$d<count($mandrill_send_data_array);$d++) {
			
			$data_stmt = mysqli_prepare($mysqli, "REPLACE INTO mandrill_send_data 
				(mandrill_send_data.ef_recipient_id,
				mandrill_send_data.ef_campaign_id,
				mandrill_send_data.mandrill_message_id,
				mandrill_send_data.mandrill_send_date,
				mandrill_send_data.mandrill_send_data_tag,
				mandrill_send_data.mandrill_send_data_opens,
				mandrill_send_data.mandrill_send_data_clicks,
				mandrill_send_data.mandrill_send_data_json,
				mandrill_send_data.mandrill_send_data_record_created_time) 
				VALUES 
				(?, ?, ?, ?, ?, ?, ?, ?, ?)");
				mysqli_stmt_bind_param($data_stmt, 'iisssiiss', $mandrill_send_data_array[$d]['ef_recipient_id'], $mandrill_send_data_array[$d]['ef_campaign_id'], $mandrill_send_data_array[$d]['mandrill_message_id'], $mandrill_send_data_array[$d]['mandrill_send_date'], $mandrill_send_data_array[$d]['mandrill_send_data_tag'], $mandrill_send_data_array[$d]['mandrill_send_data_opens'], $mandrill_send_data_array[$d]['mandrill_send_data_clicks'], $mandrill_send_data_array[$d]['mandrill_send_data_json'], $now_date_time);
				mysqli_stmt_execute($data_stmt);
			
		}
		
		
		
	} catch(Mandrill_Error $e) {
		// Mandrill errors are thrown as exceptions
		echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
		// A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
		throw $e;
	}

mysqli_stmt_free_result($stmt);
mysqli_close($stmt);
mysqli_stmt_free_result($recipient_stmt);
mysqli_close($recipient_stmt);
mysqli_stmt_free_result($data_stmt);
mysqli_close($data_stmt);
mysqli_close($mysqli);

?>