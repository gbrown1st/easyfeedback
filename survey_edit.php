<?php require('_header.php'); ?>

	<main>
		<div class="content-header">
			<div class="fluid-container">
				<div class="heading">
					<a href="#" class="back"></a>
					<h2>Edit survey</h2>
					<p>Order, add existing or create new questions</p>
				</div><!-- .heading -->

				<div class="middle-col">
					<input type="text" class="heading_input" name="survey_name" value="Feedback Survey" />
				</div><!-- .middle-col -->

				<div class="user">
					<div class="photo" style="background-image: url(images/<?php echo($_SESSION['loginImageURL']); ?>)"></div>
					<div class="text">
						<div class="name"><?php echo($_SESSION['loginName']); ?></div>
						<div class="role"><?php echo($_SESSION['loginAdminRole']); ?></div>
					</div><!-- .text -->
				</div><!-- .user -->
			</div><!-- .container -->
		</div><!-- .content-header -->

		<div class="content-main">

			<a href="#" class="btn top-button">Add question</a>

			<div class="questions_table" data-survey-id="23">
				<div class="row" data-question-id="1000">
					<div class="handle"></div>
					<div class="number"><span>1</span></div>
					<div class="title">How would you rate the service you received on your last visit?</div>
					<div class="actions">
						<div class="edit" data-hint="Edit question"></div>
						<div class="delete" data-hint="Remove"></div>
					</div>
					<div class="hint"></div>
				</div><!-- .row -->

				<div class="row" data-question-id="1001">
					<div class="handle"></div>
					<div class="number"><span>2</span></div>
					<div class="title">How likely or unlikely are you to recommend this store to your
					family and friends in the next 12 months?</div>
					<div class="actions">
						<div class="edit" data-hint="Edit question"></div>
						<div class="delete" data-hint="Remove"></div>
					</div>
					<div class="hint"></div>
				</div><!-- .row -->

				<div class="row" data-question-id="1002">
					<div class="handle"></div>
					<div class="number"><span>3</span></div>
					<div class="title">How would you rate our store presentation?</div>
					<div class="actions">
						<div class="edit" data-hint="Edit question"></div>
						<div class="delete" data-hint="Remove"></div>
					</div>
					<div class="hint"></div>
				</div><!-- .row -->

				<div class="row" data-question-id="1003">
					<div class="handle"></div>
					<div class="number"><span>4</span></div>
					<div class="title">Were you happy with our communication of recommeded
					frame and lens offers and rates?</div>
					<div class="actions">
						<div class="edit" data-hint="Edit question"></div>
						<div class="delete" data-hint="Remove"></div>
					</div>
					<div class="hint"></div>
				</div><!-- .row -->
			
			</div><!-- .questions_table -->
				
		</div><!-- .content-main -->

	</main>

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/search_form.js"></script>
	<script src="js/survey_search_ajax.js"></script>
	<script src="js/onoffswitches.js"></script>
	<script src="js/row_hints.js"></script>

	<!-- <script src="lib/draggabilly.pkgd.min.js"></script> -->
	<script src="lib/Sortable.min.js"></script>
	<script src="js/question_sort.js"></script>
</body>
</html>
