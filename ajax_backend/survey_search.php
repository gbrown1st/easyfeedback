<?php
//demo for survey search ajax, returns json (make sure returned strings are htmlencoded)

header('Content-Type: application/json');
$mysqli = new mysqli('localhost:3306', 'gbrown', 'TuscanGuitars', 'easyRecalls');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_SESSION)) {
	session_start();
}

$surgery_id = $_SESSION['loginSurgeryID'];

$stmt = mysqli_prepare($mysqli,
          "SELECT
	surveys.survey_id ,
	surveys.survey_group ,
	surveys.survey_title ,
	surveys.survey_status
FROM
	surveys
WHERE
	surveys.surgery_id = ?
ORDER BY
	surveys.survey_title ASC");

	mysqli_stmt_bind_param($stmt, 'i', $surgery_id);
    mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->survey_id, $row->survey_group, $row->survey_title, $row->survey_status);

$all_data = [];

while (mysqli_stmt_fetch($stmt)) {
	$all_data[] = ['id'=>$row->survey_id, 'name'=>$row->survey_title, 'group'=>$row->survey_group, 'active'=>($row->survey_status == 'Active' ? true : false)];
}

$search_string = $_POST['search_string'];

$return_data = [];

if( empty($search_string) ) {
	$return_data = $all_data;
}
else {
	foreach($all_data as $ad) {
		if( stripos($ad['name'], $search_string) !== false )
			$return_data[] = $ad;
	}
}

echo json_encode($return_data);
