<?php
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (isset($_GET['c'])) {
	
	session_unset();
	session_destroy();
	$_SESSION = array();
	
	session_start();
	
	$_SESSION['survey_history'] = array ();
	$_SESSION['survey_responses'] = array ();
	
	$campaign_id = (int) $_GET['c'];
	$recipient_id = (int) $_GET['r'];
	
	$_SESSION['campaign_id'] = $campaign_id;
	$_SESSION['recipient_id'] = $recipient_id;
	$_SESSION['current_pos'] = -1;
	
	$campaign_stmt = mysqli_prepare($mysqli,
          "SELECT
	campaigns_toc.campaign_name ,
	surveys_toc.survey_title ,
	campaigns_toc.survey_id ,
	campaigns_toc.survey_start_question_id ,
	brands_toc.brand_logo
FROM
	campaigns_toc
JOIN surveys_toc ON campaigns_toc.survey_id = surveys_toc.survey_id
JOIN stores_toc ON campaigns_toc.store_id = stores_toc.store_id
JOIN brands_toc ON stores_toc.brand_id = brands_toc.brand_id
WHERE
	campaign_id = ?");

	mysqli_stmt_bind_param($campaign_stmt, 'i', $campaign_id);
    mysqli_stmt_execute($campaign_stmt);

      mysqli_stmt_bind_result($campaign_stmt, $row->campaign_name, $row->survey_title, $row->survey_id, $row->survey_start_question_id, $row->brand_logo);

	while (mysqli_stmt_fetch($campaign_stmt)) {
		
		$campaign_name = $row->campaign_name;
		$survey_title = $row->survey_title;
		$survey_id = (int) $row->survey_id;
		$question_id = (int) $row->survey_start_question_id;
		$current_question_id = $question_id;
		
		$_SESSION['campaign_name'] = $campaign_name;
		$_SESSION['survey_title'] = $survey_title;
		$_SESSION['survey_id'] = $survey_id;
		$_SESSION['brand_logo'] = $row->brand_logo;
		
	}
	
	$next_link = 'Location: survey.php?surveyID='.$survey_id.'&questionID='.$question_id;
	header( $next_link ) ;
	
}