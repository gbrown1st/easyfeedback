<?php
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_SESSION)) {
	session_start();
}

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

if (!isset($_SESSION['campaign_id'])) {
	$message_array[] = array("heading"=>"Survey Session Expired.", "message"=>"This survey session has expired.");
} else {
	
$campaign_id = $_SESSION['campaign_id'];
$recipient_id = $_SESSION['recipient_id'];

$complete_stmt = mysqli_prepare($mysqli, "replace into survey_starts (campaign_id, recipient_id, start_time) values (?, ?, ?)");
mysqli_stmt_bind_param($complete_stmt, 'iis', $campaign_id, $recipient_id, $now_date_time);

if (mysqli_stmt_execute($complete_stmt)) {

   			$message_array[] = array("heading"=>"Start Recorded.", "message"=>"The start has been recorded for recipient $recipient_id for campaign $campaign_id.");
			
		} else {

			$message_array[] = array("heading"=>"Start Not Recorded.", "message"=>"The start has not been recorded.\n\nPlease try again.");

		}

	  mysqli_stmt_free_result($complete_stmt);
	  mysqli_close($stmt);
	  mysqli_close($mysqli);

}
		
echo json_encode($message_array);


?>
