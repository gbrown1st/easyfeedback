<?php
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_SESSION)) {
	session_start();
}

	if (isset($_GET['c'])) {
	
	session_unset();
	session_destroy();
	$_SESSION = array();
	
	session_start();
	
	$_SESSION['survey_history'] = array ();
	$_SESSION['survey_responses'] = array ();
	
	$campaign_id = (int) $_GET['c'];
	$recipient_id = (int) $_GET['r'];
	
	$_SESSION['campaign_id'] = $campaign_id;
	$_SESSION['recipient_id'] = $recipient_id;
	$_SESSION['current_pos'] = -1;
	
	$campaign_stmt = mysqli_prepare($mysqli,
          "SELECT
	campaigns_toc.campaign_name ,
	surveys_toc.survey_title ,
	campaigns_toc.survey_id ,
	campaigns_toc.survey_start_question_id ,
	brands_toc.brand_logo ,
	brands_toc.brand_name ,
	brands_toc.brand_email ,
	stores_toc.store_name ,
	email_recipients.RecipientFirstName ,
	email_recipients.RecipientLastName ,
	email_recipients.RecipientGender ,
	email_recipients.PractitionerName ,
	email_recipients.RecipientEmail ,
	email_recipients.RecipientLastVisitDate ,
	(
		SELECT
			questions_toc.question_description
		FROM
			questions_toc
		WHERE
			question_id = campaigns_toc.survey_intro_question_id
	) AS intro_question
FROM
	campaigns_toc
JOIN surveys_toc ON campaigns_toc.survey_id = surveys_toc.survey_id
JOIN stores_toc ON campaigns_toc.store_id = stores_toc.store_id
JOIN brands_toc ON stores_toc.brand_id = brands_toc.brand_id
JOIN campaign_recipients ON campaigns_toc.campaign_id = campaign_recipients.campaign_id
JOIN email_recipients ON campaign_recipients.recipient_id = email_recipients.RecipientID
JOIN questions_toc ON campaigns_toc.survey_start_question_id = questions_toc.question_id
WHERE
	campaigns_toc.campaign_id = ?
AND campaign_recipients.recipient_id = ?");

	mysqli_stmt_bind_param($campaign_stmt, 'ii', $campaign_id, $recipient_id);
    mysqli_stmt_execute($campaign_stmt);

      mysqli_stmt_bind_result($campaign_stmt, $row->campaign_name, $row->survey_title, $row->survey_id, $row->survey_start_question_id, $row->brand_logo, $row->brand_name, $row->brand_email, $row->store_name, $row->RecipientFirstName, $row->RecipientLastName, $row->RecipientGender, $row->PractitionerName, $row->RecipientEmail, $row->RecipientLastVisitDate, $row->intro_question);

	while (mysqli_stmt_fetch($campaign_stmt)) {
		
		$campaign_name = $row->campaign_name;
		$survey_title = $row->survey_title;
		$survey_id = (int) $row->survey_id;
		$question_id = (int) $row->survey_start_question_id;
		$intro_question = $row->intro_question;
		
		$_SESSION['campaign_name'] = $campaign_name;
		$_SESSION['survey_title'] = $survey_title;
		$_SESSION['survey_id'] = $survey_id;
		$_SESSION['store_name'] = $row->store_name;
		$_SESSION['brand_logo'] = $row->brand_logo;
		$_SESSION['brand_name'] = $row->brand_name;
		$_SESSION['brand_email'] = $row->brand_email;
		$_SESSION['first_name'] = $row->RecipientFirstName;
		$_SESSION['last_name'] = $row->RecipientLastName;
		$_SESSION['recipient_email'] = $row->RecipientEmail;
		$_SESSION['last_visit'] = $row->RecipientLastVisitDate;
		$_SESSION['gender'] = $row->RecipientGender;
		$_SESSION['practitioner_name'] = $row->PractitionerName;
		
		$jsonString = urldecode($row->intro_question);
		$data = json_decode($jsonString, true);
		$question_text = $data['question_text'];
		$question_text = str_replace('{First Name}', $_SESSION['first_name'], $question_text);
		$question_text = str_replace('{Store Name}', $_SESSION['store_name'], $question_text);
		$question_text = str_replace('The The ', 'The ', $question_text);
		$question_text = str_replace('{Optometrist Name}', $_SESSION['practitioner_name'], $question_text);
		$question_html = '<div class="question">'.$question_text.'</div>';
		
	}
		
		
		
		if ($_SESSION['gender'] == 'F') {
			$_SESSION['bg_image'] = 'images/toc-woman.jpg';
		} else if ($_SESSION['gender'] == 'M') {
			$_SESSION['bg_image'] = 'images/toc-man.jpg';
		} else {
			$_SESSION['bg_image'] = 'none';
		}
		
	}
		
?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $survey_title;?></title>

	<link href="https://fonts.googleapis.com/css?family=Montserrat|Rubik" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    
    <script type="text/javascript">
 
$(document).ready(function () {

$('#startButton').click(function (e) {
	e.preventDefault();
	request.open("POST", "start.php", false);
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	request.send();
	var start_url = '/survey/survey.php?surveyID='+<?php echo $survey_id;?>+'&questionID='+<?php echo $question_id;?>;
	window.location.replace(start_url);
})
	

});

var request = false;
   try {
     request = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       request = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         request = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         request = false;
       }  
     }
   }
		
</script>
</head>
<body>
	<main>
		<h1 class="client_logo" style="background-image: url(images/logo/<?php echo $_SESSION['brand_logo'];?>)"><span class="hidden"></span></h1>
<?php echo $question_html;?>
	
			<div class="buttons" style="margin-top:50px;">
				<a href="#" id="startButton" class="btn btn-primary btn-start">Begin survey</a>
			</div>
			
	</main>
	<?php
	if ($_SESSION['bg_image'] == 'none') {
		if(mt_rand(0,1) == 0) {
		?>
			<img src="images/toc-woman.jpg" alt="" class="bg" />
		<?php	
		} else {
			?>
			<img src="images/toc-man.jpg" alt="" class="bg" />
		<?php	
		}
   ?>
   <?php
	} else {
		?>
			<img src="<?php echo $_SESSION['bg_image'];?>" alt="" class="bg" />
		<?php
	}
	?>
	<script src="js/jquery-3.1.1.min.js"></script>
</body>
</html>

