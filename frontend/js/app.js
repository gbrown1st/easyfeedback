$(function() {

	var progress = $('.progress');
	var current = $('#current');
	var total_count = parseInt( $('#total').text() );
	var one_step_width = 100 / (total_count);
	var current_step = 1;

	progress.width( one_step_width );

	$('.btn-next').on('click tap', function(e) {
		e.preventDefault();
		
		if( current_step < total_count ) {
			current_step++;
			current.text( current_step );
			var width = current_step * one_step_width;
			progress.width( width + '%');
		}

	});

	$('.btn-back').on('click tap', function(e) {
		e.preventDefault();

		if( current_step > 1 ) {
			current_step--;
			current.text( current_step );
			var width = current_step * one_step_width;
			progress.width( width + '%');
		}
	});

});
