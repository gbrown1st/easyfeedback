<?php
require_once '../mandrill-api-php/src/Mandrill.php';
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_SESSION)) {
	session_start();
}

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');

if (!isset($_SESSION['campaign_id'])) {
	$message_array[] = array("heading"=>"Survey Session Expired.", "message"=>"This survey session has expired.");
	$complete_message = "This survey session has expired.";
} else {
	
$campaign_id = $_SESSION['campaign_id'];
$recipient_id = $_SESSION['recipient_id'];
$sname = $_SESSION['store_name'];
$logo_src = 'https://easyfeedback.myhealth1st.com.au/survey/images/logo/'.$_SESSION['brand_logo'];
$brand_name = $_SESSION['brand_name'];
$brand_email = $_SESSION['brand_email'];
$fname = $_SESSION['first_name'];
$lname = $_SESSION['last_name'];
$dateLastAppt = $_SESSION['last_visit'];
$pEmailAddress = $_SESSION['recipient_email'];
$gender = $_SESSION['gender'];
$oname = $_SESSION['practitioner_name'];
$to = array();
$to[] = array(
                'email' => $pEmailAddress,
                'name' => $fname.' '.$lname,
                'type' => 'to'
);

$complete_stmt = mysqli_prepare($mysqli, "replace into survey_completions (campaign_id, recipient_id, completion_time) values (?, ?, ?)");
mysqli_stmt_bind_param($complete_stmt, 'iis', $campaign_id, $recipient_id, $now_date_time);

if (mysqli_stmt_execute($complete_stmt)) {

   			$message_array[] = array("heading"=>"Completion Recorded.", "message"=>"Thank you. Your voucher has been sent.");
			$complete_message = "Thank you. Your voucher has been sent.";
	
		} else {

			$message_array[] = array("heading"=>"Completion Not Recorded.", "message"=>"The completion has not been recorded.\n\nPlease try again.");
			$complete_message = "There has been an error. Your voucher could not be sent.";

		}

	  mysqli_stmt_free_result($complete_stmt);
	  //mysqli_close($stmt);
	  //mysqli_close($mysqli);
session_unset();
session_destroy();
$_SESSION = array();
	
}

try {
    $mandrill = new Mandrill('g-WNr3dvxNEpsC5dkKCoVA');
    $template_name = 'easyfeedback-thank-you';
    $template_content = [];
    $message = array(
        'subject' => 'Thanks for your opinion',
        'from_email' => $brand_email,
        'from_name' => $sname.' Feedback',
        'to' => $to,
        'headers' => array('Reply-To' => $brand_email),
        'important' => false,
        'track_opens' => null,
        'track_clicks' => null,
        'auto_text' => null,
        'auto_html' => null,
        'inline_css' => null,
        'url_strip_qs' => null,
        'preserve_recipients' => null,
        'view_content_link' => null,
        'tracking_domain' => null,
        'signing_domain' => null,
        'return_path_domain' => null,
        'merge' => true,
        'merge_language' => 'handlebars',
        'global_merge_vars' => array(
            array(
                'name' => 'logo_src',
                'content' => $logo_src
            ),
			array(
                'name' => 'fname',
                'content' => $fname
            ),
			array(
                'name' => 'sname',
                'content' => $sname
            ),
			array(
                'name' => 'oname',
                'content' => $oname
            ),
			array(
                'name' => 'dateLastAppt',
                'content' => $dateLastAppt
            ),
			array(
                'name' => 'pEmailAddress',
                'content' => $pEmailAddress
            ),
			array(
                 'name' => 'unsubscribeURL',
                  'content' => 'https://easyfeedback.myhealth1st.com.au/unsubscribe.php?c='.$campaign_id.'&r='.$recipient_id
            )
        )
    );
	
    $async = true;
    $ip_pool = 'Main Pool';
    $send_at = '2017-02-06';
    $result = $mandrill->messages->sendTemplate($template_name, $template_content, $message, $async, $ip_pool, $send_at);
    //print_r($result);
    /*
    Array
    (
        [0] => Array
            (
                [email] => recipient.email@example.com
                [status] => sent
                [reject_reason] => hard-bounce
                [_id] => abc123abc123abc123abc123abc123
            )
    
    )
    */
} catch(Mandrill_Error $e) {
    // Mandrill errors are thrown as exceptions
    echo 'A mandrill error occurred: ' . get_class($e) . ' - ' . $e->getMessage();
    // A mandrill error occurred: Mandrill_Unknown_Subaccount - No subaccount exists with the id 'customer-123'
    throw $e;
}
			
echo $complete_message;


?>
