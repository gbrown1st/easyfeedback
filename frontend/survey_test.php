<?php
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_SESSION)) {
	session_start();
}

if (isset($_GET['c'])) {
	
	$campaign_id = (int) $_GET['c'];
	$recipient_id = (int) $_GET['r'];
	
	$campaign_stmt = mysqli_prepare($mysqli,
          "SELECT
	campaigns_toc.campaign_name ,
	surveys_toc.survey_title ,
	campaigns_toc.survey_id ,
	campaigns_toc.survey_start_question_id
FROM
	campaigns_toc
JOIN surveys_toc ON campaigns_toc.survey_id = surveys_toc.survey_id
WHERE
	campaign_id = ?");

	mysqli_stmt_bind_param($campaign_stmt, 'i', $campaign_id);
    mysqli_stmt_execute($campaign_stmt);

      mysqli_stmt_bind_result($campaign_stmt, $row->campaign_name, $row->survey_title, $row->survey_id, $row->survey_start_question_id);

	while (mysqli_stmt_fetch($campaign_stmt)) {
		
		$campaign_name = $row->campaign_name;
		$survey_title = $row->survey_title;
		$survey_id = (int) $row->survey_id;
		$question_id = (int) $row->survey_start_question_id;
		$current_question_id = $question_id;
		
		$_SESSION['campaign_name'] = $campaign_name;
		$_SESSION['campaign_id'] = $campaign_id;
		$_SESSION['survey_title'] = $survey_title;
		$_SESSION['survey_id'] = $survey_id;
		$_SESSION['recipient_id'] = $recipient_id;
		
	}
	
} else {
	
	$survey_id = (int) $_GET['surveyID'];
	$current_question_id = (int) $_GET['questionID'];
	$question_id = (int) $_GET['questionID'];
	
}



$stmt = mysqli_prepare($mysqli,
          "SELECT
	surveys_toc.survey_title ,
	questions_toc.question_description ,
	survey_questions_toc.question_id ,
	survey_questions_toc.previous_question_id ,
	survey_questions_toc.next_question_id ,
	survey_questions_toc.branches ,
	survey_questions_toc.branch_next_question_id ,
	survey_questions_toc.current_step ,
	survey_questions_toc.total_steps
FROM
	survey_questions_toc
JOIN surveys_toc ON survey_questions_toc.survey_id = surveys_toc.survey_id
JOIN questions_toc ON survey_questions_toc.question_id = questions_toc.question_id
WHERE
	survey_questions_toc.survey_id = ?
AND survey_questions_toc.question_id = ?");

	mysqli_stmt_bind_param($stmt, 'ii', $survey_id, $question_id);
    mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->survey_title, $row->question_description, $row->question_id, $row->previous_question_id, $row->next_question_id, $row->branches, $row->branch_next_question_id, $row->current_step, $row->total_steps);


while (mysqli_stmt_fetch($stmt)) {
	$survey_title = $row->survey_title;
	$question_description = $row->question_description;
	$jsonString = urldecode($row->question_description);
	$data = json_decode($jsonString, true);
	$question_type_id = $data['question_type_id'];
	$question_text = $data['question_text'];
	//print_r($data);
	$branches = $row->branches;
	$question_id = (int) $row->next_question_id;
	$previous_question_id = (int) $row->previous_question_id;
	$branch_question_id = (int) $row->branch_next_question_id;
	$current_step = (int) $row->current_step;
	$total_steps = (int) $row->total_steps;
	$next_link = 'survey.php?surveyID='.$survey_id.'&questionID='.$question_id;
	$back_link = 'survey.php?surveyID='.$survey_id.'&questionID='.$previous_question_id;
	if ($branches == 'n') {
		$next_branch_link = 'survey.php?surveyID='.$survey_id.'&questionID='.$question_id;
	} else {
		$next_branch_link = 'survey.php?surveyID='.$survey_id.'&questionID='.$branch_question_id;
	}
	
	switch ($question_type_id) {
		
		case 'Free text':
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer textarea">
			<textarea id="q_'.$row->question_id.'" rows="10"></textarea>
		</div>';
			
			$question_html .= '
		</div><!-- .answer.full-width -->';
			break;
			
		case 'Yes/No':
			$question_text = str_replace("#Major Campaign#", $_SESSION['campaign_name'], $question_text);
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer full-width">
			<div class="mobile_row">';
				$question_html .= '
			<label class="tick">
				<input type="radio" id="branch_yes" name="q_'.$row->question_id.'" value="yes" />
				<span class="check"></span>
				<span class="text">Yes</span>
			</label>
		
			<label class="tick">
				<input type="radio" id="branch_no" name="q_'.$row->question_id.'" value="no" />
				<span class="check"></span>
				<span class="text">No</span>
			</label>';
			
			$question_html .= '</div><!-- .mobile_row -->
		</div><!-- .answer.full-width -->';
			break;
			
		case '5 point scale':
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer full-width">
			<div class="mobile_row">';
			for ($i=1;$i<6;$i++){
				$question_html .= '
				<label class="number">
					<input type="radio" name="q_'.$row->question_id.'" value="'.$i.'" />
					<span class="circle">'.$i.'</span>
				</label>';
			}
			
			$question_html .= '</div><!-- .mobile_row -->
		</div><!-- .answer.full-width -->';
			break;
			
		case '10 point scale':
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer full-width">
			<div class="mobile_row">';
			for ($i=0;$i<6;$i++){
				$question_html .= '
				<label class="number">
					<input type="radio" name="q_'.$row->question_id.'" value="'.$i.'" />
					<span class="circle">'.$i.'</span>
				</label>';
			}
			$question_html .= '</div><!-- .mobile_row -->';
			$question_html .= '<div class="mobile_row">';
			for ($i=6;$i<11;$i++){
				$question_html .= '
				<label class="number">
					<input type="radio" name="q_'.$row->question_id.'" value="'.$i.'" />
					<span class="circle">'.$i.'</span>
				</label>';
			}
			$question_html .= '</div><!-- .mobile_row -->
			</div><!-- .answer.full-width -->';
			break;
			
			case 'Radio buttons':
			$scale_descriptors = $data['scale_descriptors'];
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer full-width">
			<div class="mobile_row">';
			for ($i=0;$i<count($scale_descriptors);$i++){
				if (is_array($scale_descriptors[$i])) {
					$allKeys = array_keys($scale_descriptors[$i]);
					$allValues = array_values($scale_descriptors[$i]);
					//print_r($allKeys);
					//print_r($allValues);
					if (is_array($allValues[0])) {
						$sub_question_type_id = $allValues[0]['question_type_id'];
						$sub_question_text = $allValues[0]['question_text'];
						switch ($sub_question_type_id) {
							case '5 point scale':
								$sub_question_html = '<div class="question">'.$question_text.'</div>
								<div class="answer full-width">
									<div class="mobile_row">';
									for ($i=1;$i<6;$i++){
										$sub_question_html .= '
										<label class="number">
											<input type="radio" name="q_'.$row->question_id.'" value="'.$i.'" />
											<span class="circle">'.$i.'</span>
										</label>';
									}
			
								$sub_question_html .= '</div><!-- .mobile_row -->
							</div><!-- .answer.full-width -->';
							break;
						}
						
					} else {
						
					}
					$question_html .= '
				<label class="radio">
				<input type="radio" name="q_'.$row->question_id.'" value="" />
				<span class="circle"><span class="inner"></span></span>
				<span class="text">'.$allKeys[0].'</span>
			</label>';
					$question_html.= $sub_question_html;
				} else {
					$question_html .= '
				<label class="radio">
				<input type="radio" name="q_'.$row->question_id.'" value="'.$scale_descriptors[$i].'" />
				<span class="circle"><span class="inner"></span></span>
				<span class="text">'.$scale_descriptors[$i].'</span>
			</label>';
				}
			}
			
			$question_html .= '</div><!-- .mobile_row -->
		</div><!-- .answer.full-width -->';
			break;
			
			case 'Multiple choice':
			$scale_descriptors = $data['scale_descriptors'];
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer full-width">
			<div class="mobile_row">';
			for ($i=0;$i<count($scale_descriptors);$i++){
				if (is_array($scale_descriptors[$i])) {
					$allKeys = array_keys($scale_descriptors[$i]);
					$allValues = array_values($scale_descriptors[$i]);
					if (is_array($allValues[0])) {
						$sub_question_id = $allValues[0]['sub_question_id'];
					}
					$question_html .= '
			<label class="tick">
				<input type="checkbox" name="q_'.$row->question_id.'[]" data-ref="'.$sub_question_id.'" value="'.$allKeys[0].'" />
				<span class="check"></span>
				<span class="text">'.$allKeys[0].'</span>
			</label>
			';
				} else {
					$question_html .= '
			<label class="tick">
				<input type="checkbox" name="q_'.$row->question_id.'[]" data-ref="0" value="'.$scale_descriptors[$i].'" />
				<span class="check"></span>
				<span class="text">'.$scale_descriptors[$i].'</span>
			</label>';
				}
			}
			
			$question_html .= '</div><!-- .mobile_row -->
		</div><!-- .answer.full-width -->';
			break;
	}
}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $survey_title;?></title>

	<link href="https://fonts.googleapis.com/css?family=Montserrat|Rubik" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    
    <script type="text/javascript">
 
$(document).ready(function () {

var progress = $('.progress');
var current = $('#current');
var total = $('#total');
var one_step_width = 100 / (<?php echo $total_steps;?>);
current.text( "<?php echo $current_step;?>" );
total.text( "<?php echo $total_steps;?>" );
var width = <?php echo $current_step;?> * one_step_width;
progress.width( width + '%');
	
var question_type_id = "<?php echo $question_type_id;?>";
var response_question_id = "q_"+"<?php echo $current_question_id;?>";
var previous_question_id = <?php echo $previous_question_id;?>;

if (previous_question_id == 0) {
	$("backButton").attr("disabled", true);
}
$('#backButton').click(function (e) {
	e.preventDefault();
	var back_url = "<?php echo $back_link;?>";
	if (previous_question_id > 0) {
		window.location.replace(back_url);
	}
})

$('#nextButton').click(function (e) {
	e.preventDefault();
	
	var has_sub_questions = false;
	
	var q_array = response_question_id.split("_");
	var q = q_array[1];
	var t = "<?php echo $question_text;?>";
	
	switch (question_type_id) {
		case "Yes/No":
			var a = $("input:radio[name='"+response_question_id+"']:checked").val();
			break;
		case "5 point scale":
			var a = $("input:radio[name='"+response_question_id+"']:checked").val();
			break;
		case "10 point scale":
			var a = $("input:radio[name='"+response_question_id+"']:checked").val();
			break;
		case "Radio buttons":
			var a = $("input:radio[name='"+response_question_id+"']:checked").val();
			break;
		case "Multiple choice":
			
			var values = new Array();
			var sub_question_array = new Array();
			
$.each($("input[name='"+response_question_id+"[]']:checked"), function() {
  values.push($(this).val());
	var sub_question_id = $(this).attr('data-ref');
	if (sub_question_id != "0") {
		sub_question_array.push(sub_question_id);
		has_sub_questions = true;
	}
  // or you can do something to the actual checked checkboxes by working directly with  'this'
  // something like $(this).hide() (only something useful, probably) :P
});
			var a = JSON.stringify(values);
			
			break;
		case "Free text":
			var textAreaID = "#"+response_question_id;
			var a = $(textAreaID).val();
			break;
	}
	
	var c = <?php echo $_SESSION['campaign_id'];?>;
	var r = <?php echo $_SESSION['recipient_id'];?>;
	
	request.open("POST", "response_process.php", false);
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	request.send("q="+q+"&a="+a+"&t="+t+"&r="+r+"&c="+c);
	
	
	var url = "<?php echo $next_link;?>";
	
	if (has_sub_questions) {
		url = "survey.php?surveyID="+<?php echo $survey_id;?>+"&questionID="+sub_question_array[0];
	}
	
	var branches = "<?php echo $branches;?>";
	
	var branch_url = "<?php echo $next_branch_link;?>";
	if (branches == 'n') {
		window.location.replace(url);
	} else {
		if($('#branch_yes').is(':checked')) { 
			alert();
			window.location.replace(url);
		} else if($('#branch_no').is(':checked')) { 
			window.location.replace(branch_url);
		} else {
			//alert("Please choose yes or no");
		}
	}
})


});
		
var request = false;
   try {
     request = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       request = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         request = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         request = false;
       }  
     }
   }
		
		</script>
</head>
<body>
	<main>
		<h1 class="client_logo" style="background-image: url(images/client_logo.gif)"><span class="hidden"></span></h1>
<?php echo $question_html;?>
		<div class="buttons">
			<a href="#" id="backButton" class="btn btn-back">Back</a>
			<a href="#" id="nextButton" class="btn btn-primary btn-next">Next</a>
		</div>

		<div class="progress-holder">
			<div class="progress-bar">
				<div class="progress" style="width: 1%;"></div>
			</div>
			<div class="text">
			  <p><span id="current">1</span> of <span id="total">11</span> answered			</p>
			</div>
		</div><!-- .progress-holder -->
		</div><!-- .progress-holder -->
	</main>
	<img src="images/client-image-1.jpg" alt="" class="bg" />

	<script src="js/jquery-3.1.1.min.js"></script>
</body>
</html>
