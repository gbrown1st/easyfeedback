<?php
if (!isset($_SESSION)) {
	session_start();
}

$pos = $_SESSION['current_pos'] - 1;
if ($pos < 0) {
	$pos = 0;
}
$_SESSION['current_pos'] = $pos;
$url = $_SESSION['survey_history'][$pos];
echo $url;
?>