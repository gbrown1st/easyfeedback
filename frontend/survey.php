<?php
$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_SESSION)) {
	session_start();
}

	if (!in_array($_SERVER['REQUEST_URI'], $_SESSION['survey_history'])) {
		array_push($_SESSION['survey_history'],$_SERVER['REQUEST_URI']);
	}
	
	$brand_logo = $_SESSION['brand_logo'];

	$_SESSION['current_pos'] = array_search($_SERVER['REQUEST_URI'], $_SESSION['survey_history']);
	
	$survey_id = (int) $_GET['surveyID'];
	$current_question_id = (int) $_GET['questionID'];
	$question_id = (int) $_GET['questionID'];

$current_answer = $_SESSION['survey_responses'][$question_id];

$sq_array = isset($_REQUEST['sq']) ? json_decode($_REQUEST['sq']) : array();

$stmt = mysqli_prepare($mysqli,
          "SELECT
	surveys_toc.survey_title ,
	questions_toc.question_description ,
	survey_questions_toc.question_id ,
	survey_questions_toc.previous_question_id ,
	survey_questions_toc.next_question_id ,
	survey_questions_toc.branches ,
	survey_questions_toc.branch_next_question_id ,
	survey_questions_toc.current_step ,
	survey_questions_toc.total_steps
FROM
	survey_questions_toc
JOIN surveys_toc ON survey_questions_toc.survey_id = surveys_toc.survey_id
JOIN questions_toc ON survey_questions_toc.question_id = questions_toc.question_id
WHERE
	survey_questions_toc.survey_id = ?
AND survey_questions_toc.question_id = ?");

	mysqli_stmt_bind_param($stmt, 'ii', $survey_id, $question_id);
    mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->survey_title, $row->question_description, $row->question_id, $row->previous_question_id, $row->next_question_id, $row->branches, $row->branch_next_question_id, $row->current_step, $row->total_steps);


while (mysqli_stmt_fetch($stmt)) {
	$survey_title = $row->survey_title;
	$question_description = $row->question_description;
	$jsonString = urldecode($row->question_description);
	$data = json_decode($jsonString, true);
	$question_type_id = $data['question_type_id'];
	$question_text = $data['question_text'];
	//print_r($data);
	$branches = $row->branches;
	$question_id = (int) $row->next_question_id;
	$branch_question_id = (int) $row->branch_next_question_id;
	if (isset($_REQUEST['cs'])) {
		$current_step = $_REQUEST['cs'];
	} else {
		$current_step = (int) $row->current_step;
	}
	$total_steps = (int) $row->total_steps;
	$next_link = 'survey.php?surveyID='.$survey_id.'&questionID='.$question_id;
	if (count($sq_array) > 0) {
		$sq_next = $sq_array[0];
		array_shift($sq_array);
		$sq_array_serialized = json_encode($sq_array);
		if (count($sq_array) == 0) {
			$next_link = '/survey/survey.php?surveyID='.$survey_id.'&questionID='.$sq_next;
		} else {
			$next_link = '/survey/survey.php?surveyID='.$survey_id.'&questionID='.$sq_next.'&sq='.$sq_array_serialized.'&cs='.$current_step;
		}
	} 
	if ($branches == 'n') {
		$next_branch_link = '/survey/survey.php?surveyID='.$survey_id.'&questionID='.$question_id;
	} else {
		$next_branch_link = '/survey/survey.php?surveyID='.$survey_id.'&questionID='.$branch_question_id;
	}
	
	switch ($question_type_id) {
		
		case 'Free text':
			$question_html = '<div class="question">'.$question_text.'</div><div class="question" style="font-size: 100%; padding-top:10px;">(you don\'t have to put anything in but it would be helpful)</div>
		<div class="answer textarea">
			<textarea id="q_'.$row->question_id.'" rows="10"></textarea>
		</div>';
			
			$question_html .= '
		</div>';
			break;
			
		case 'Yes/No':
			$question_text = str_replace("#Major Campaign#", $_SESSION['campaign_name'], $question_text);
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer">
			<div class="mobile_row">';
				$question_html .= '
			<label class="tick">
				<input type="radio" id="branch_yes" name="q_'.$row->question_id.'" value="yes" />
				<span class="check"></span>
				<span class="text">Yes</span>
			</label>
		
			<label class="tick">
				<input type="radio" id="branch_no" name="q_'.$row->question_id.'" value="no" />
				<span class="check"></span>
				<span class="text">No</span>
			</label>';
			
			$question_html .= '</div><!-- .mobile_row -->
		</div>';
			break;
			
		case '5 point scale':
			$scale_descriptors = $data['scale_descriptors'];
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer full-width">
			<div class="mobile_row">';
			for ($i=0;$i<count($scale_descriptors);$i++){
				$question_html .= '
				<label class="tick">
					<input type="radio" name="q_'.$row->question_id.'" value="'.(5-$i).'" />
					<span class="check"></span>
					<span class="text">'.$scale_descriptors[$i].'</span>
				</label>';
			}
			
			$question_html .= '</div><!-- .mobile_row -->
		</div><!-- .answer.full-width -->';
			break;
			
		case '10 point scale':
			$question_html = '<div class="question">'.$question_text.'</div><div class="question" style="font-size: 100%; padding-top:10px;">(10 is highly likely and 0 is highly unlikely)</div>
		<div class="answer full-width">
			<div class="mobile_row">';
			for ($i=0;$i<6;$i++){
				$question_html .= '
				<label class="number">
					<input type="radio" name="q_'.$row->question_id.'" value="'.$i.'" />
					<span class="circle">'.$i.'</span>
				</label>';
			}
			$question_html .= '</div><!-- .mobile_row -->';
			$question_html .= '<div class="mobile_row">';
			for ($i=6;$i<11;$i++){
				$question_html .= '
				<label class="number">
					<input type="radio" name="q_'.$row->question_id.'" value="'.$i.'" />
					<span class="circle">'.$i.'</span>
				</label>';
			}
			$question_html .= '</div><!-- .mobile_row -->
			</div><!-- .answer.full-width -->';
			break;
			
			case 'Radio buttons':
			$scale_descriptors = $data['scale_descriptors'];
			$question_html = '<div class="question">'.$question_text.'</div>
		<div class="answer full-width">
			<div class="mobile_row">';
			for ($i=0;$i<count($scale_descriptors);$i++){
				if (is_array($scale_descriptors[$i])) {
					$allKeys = array_keys($scale_descriptors[$i]);
					$allValues = array_values($scale_descriptors[$i]);
					if (is_array($allValues[0])) {
						$sub_question_id = $allValues[0]['sub_question_id'];
					}
					
					$question_html .= '
				<label class="radio">
				<input type="radio" name="q_'.$row->question_id.'[]" data-ref="'.$sub_question_id.'" value="'.$allKeys[0].'" />
				<span class="circle"><span class="inner"></span></span>
				<span class="text">'.$allKeys[0].'</span>
			</label>';
					
				} else {
					$question_html .= '
				<label class="radio">
				<input type="radio" name="q_'.$row->question_id.'[]" data-ref="0" value="'.$scale_descriptors[$i].'" />
				<span class="circle"><span class="inner"></span></span>
				<span class="text">'.$scale_descriptors[$i].'</span>
			</label>';
				}
			}
			
			$question_html .= '</div><!-- .mobile_row -->
		</div><!-- .answer.full-width -->';
			break;
			
			case 'Multiple choice':
			$scale_descriptors = $data['scale_descriptors'];
			$question_html = '<div class="question">'.$question_text.'</div><div class="question" style="font-size: 100%; padding-top:10px;">(choose as many answers as you want)</div>
		<div class="answer full-width">
			<div class="mobile_row">';
			for ($i=0;$i<count($scale_descriptors);$i++){
				if (is_array($scale_descriptors[$i])) {
					$allKeys = array_keys($scale_descriptors[$i]);
					$allValues = array_values($scale_descriptors[$i]);
					if (is_array($allValues[0])) {
						$sub_question_id = $allValues[0]['sub_question_id'];
					}
					$question_html .= '
			<label class="tick">
				<input type="checkbox" name="q_'.$row->question_id.'[]" data-ref="'.$sub_question_id.'" value="'.$allKeys[0].'" />
				<span class="check"></span>
				<span class="text">'.$allKeys[0].'</span>
			</label>
			';
				} else {
					$question_html .= '
			<label class="tick">
				<input type="checkbox" name="q_'.$row->question_id.'[]" data-ref="0" value="'.$scale_descriptors[$i].'" />
				<span class="check"></span>
				<span class="text">'.$scale_descriptors[$i].'</span>
			</label>';
				}
			}
			
			$question_html .= '</div>
		</div>';
			break;
			
			case 'Completion':
			$question_html = '<div class="question">'.$question_text.'</div>';
			
			break;
	}
}

?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php echo $survey_title;?></title>

	<link rel="stylesheet" type="text/css" href="css/styles.css" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    
    <script type="text/javascript">
 
$(document).ready(function () {

var progress = $('.progress');
var current = $('#current');
var total = $('#total');
var one_step_width = 100 / (<?php echo $total_steps;?>);
current.text( "<?php echo $current_step;?>" );
total.text( "<?php echo $total_steps;?>" );
var width = <?php echo $current_step;?> * one_step_width;
progress.width( width + '%');
	
var question_type_id = "<?php echo $question_type_id;?>";

var response_question_id = "q_"+"<?php echo $current_question_id;?>";
var current_answer = '<?php echo $current_answer;?>';
var answerJSON = [];
if (current_answer.indexOf("[") > -1) {
	var answerJSON = JSON.parse(current_answer);
	current_answer = "";
}

switch (question_type_id.valueOf()) {
		case "Yes/No":
			$("input:radio[name='"+response_question_id+"'][value='"+current_answer+"']").attr('checked',true);
			break;
		case "5 point scale":
			$("input:radio[name='"+response_question_id+"'][value='"+current_answer+"']").attr('checked',true);
			break;
		case "10 point scale":
			$("input:radio[name='"+response_question_id+"'][value='"+current_answer+"']").attr('checked',true);
			break;
		case "Radio buttons":
			$("input:radio[name='"+response_question_id+"[]'][value='"+current_answer+"']").attr('checked',true);
			break;
		case "Free text":
			var textAreaID = "textarea#"+response_question_id;
			$(textAreaID).val(current_answer);
			break;
		case "Multiple choice":
				for(var a in answerJSON) {
				   	current_answer = answerJSON[a];
					$("input:checkbox[name='"+response_question_id+"[]'][value='"+current_answer+"']").attr('checked',true);
				}
			break;
	}

$('#finishButton').click(function (e) {
	e.preventDefault();
	request.open("POST", "complete.php", false);
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	request.send();
	var strResult=request.responseText;
	alert(strResult);
})
	
$('#backButton').click(function (e) {
	e.preventDefault();
	request.open("POST", "goback.php", false);
	request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	request.send();
	var back_url=request.responseText;
	window.location.replace(back_url);
})

$('#nextButton').click(function (e) {
	e.preventDefault();
	
	var has_sub_questions = false;
	
	var q_array = response_question_id.split("_");
	var q = q_array[1];
	var t = "<?php echo $question_text;?>";
	
	switch (question_type_id) {
		case "Yes/No":
			var a = $("input:radio[name='"+response_question_id+"']:checked").val();
			break;
		case "5 point scale":
			var a = $("input:radio[name='"+response_question_id+"']:checked").val();
			break;
		case "10 point scale":
			var a = $("input:radio[name='"+response_question_id+"']:checked").val();
			break;
		case "Radio buttons":
			var a = $("input:radio[name='"+response_question_id+"[]']:checked").val();
			var sub_question_array = new Array();
			$.each($("input[name='"+response_question_id+"[]']:checked"), function() {
				var sub_question_id = parseInt($(this).attr('data-ref'));
				if (sub_question_id > 0) {
					sub_question_array.push(sub_question_id);
					has_sub_questions = true;
				}
			});
			break;
		case "Multiple choice":
			
			var values = new Array();
			var sub_question_array = new Array();
			$.each($("input[name='"+response_question_id+"[]']:checked"), function() {
			  values.push($(this).val());
				var sub_question_id = parseInt($(this).attr('data-ref'));
				if (sub_question_id > 0) {
					sub_question_array.push(sub_question_id);
					has_sub_questions = true;
				}
			});
			var a = JSON.stringify(values);
			
			break;
		case "Free text":
			var textAreaID = "#"+response_question_id;
			var a = $(textAreaID).val();
			if (!a || a == "" || a.length == 0 ) {
				a = "###";
			}
			break;
	}
	
	if( a && a.length > 0 && a != "[]") {
		
		if (a == "###") {
			a = "";
		}
		
		var c = <?php echo $_SESSION['campaign_id'];?>;
		var r = <?php echo $_SESSION['recipient_id'];?>;

		request.open("POST", "response_process.php", false);
		request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		request.send("q="+q+"&a="+a+"&t="+t+"&r="+r+"&c="+c);

		var branches = "<?php echo $branches;?>";
		var url = "<?php echo $next_link;?>";
		if (has_sub_questions) {
			if (sub_question_array.length == 1) {
				url = '/survey/survey.php?surveyID='+<?php echo $survey_id;?>+'&questionID='+sub_question_array[0]+'&cs='+<?php echo $current_step;?>;
			} else {
				var first_sub_question = sub_question_array[0];
				sub_question_array.shift();
				var s = JSON.stringify(sub_question_array);
				url = '/survey/survey.php?surveyID='+<?php echo $survey_id;?>+'&questionID='+first_sub_question+'&sq=' + s+'&cs='+<?php echo $current_step;?>;
			}
		}

		var branch_url = "<?php echo $next_branch_link;?>";
		if (branches == 'n') {
			
			window.location.replace(url);
			
		} else {
			
			if($('#branch_yes').is(':checked')) { 
				window.location.replace(url);
			} else if($('#branch_no').is(':checked')) { 
				window.location.replace(branch_url);
			}
			
		}
		
	} else {
		
		alert("Please enter a response.");
		
	}
	
	
})


});
		
var request = false;
   try {
     request = new XMLHttpRequest();
   } catch (trymicrosoft) {
     try {
       request = new ActiveXObject("Msxml2.XMLHTTP");
     } catch (othermicrosoft) {
       try {
         request = new ActiveXObject("Microsoft.XMLHTTP");
       } catch (failed) {
         request = false;
       }  
     }
   }
		
		</script>
</head>
<body>
	<main>
		<h1 class="client_logo" style="background-image: url(images/logo/<?php echo $brand_logo;?>)"><span class="hidden"></span></h1>
<?php echo $question_html;?>
	<?php
		if ($question_type_id == 'Completion') {
			?>
			<div class="buttons" style="margin-top:50px;">
				<a href="#" id="finishButton" class="btn btn-primary btn-end">Send my voucher</a>
			</div>
			<?php
		} else {
		?>
		<div class="buttons">
		
		<?php
		
			if ($_SESSION['current_pos'] > 0) {
				
		?>
			<a href="#" id="backButton" class="btn btn-back">Back</a>
			
			<?php
		}
		?>
			<a href="#" id="nextButton" class="btn btn-primary btn-next">Next</a>
		</div>

		<div class="progress-holder">
			<div class="progress-bar">
				<div class="progress" style="width: 1%;"></div>
			</div>
			<div class="text">
			  <p><span id="current">1</span> of <span id="total">11</span> answered			</p>
			</div>
		</div><!-- .progress-holder -->
		</div><!-- .progress-holder -->
		<?php
		}
		?>
	</main>
	<?php
	if(mt_rand(0,1) == 0) {
		?>
		<img src="images/toc-woman.jpg" alt="" class="bg" />
	<?php	
	} else {
		?>
		<img src="images/toc-man.jpg" alt="" class="bg" />
	<?php	
	}
   ?>
	<script src="js/jquery-3.1.1.min.js"></script>
</body>
</html>
