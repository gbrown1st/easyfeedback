<?php
$mysqli = new mysqli('localhost:3306', 'gbrown', 'TuscanGuitars', 'easyRecalls');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_GET['surveyID'])) {
	$survey_id = 1;
	$question_id = 1;
} else {
	$survey_id = (int) $_GET['surveyID'];
	$question_id = (int) $_GET['questionID'];
}

$stmt = mysqli_prepare($mysqli,
          "SELECT
	surveys_toc.survey_title ,
	questions_toc.question_description ,
	survey_questions_toc.question_id ,
	survey_questions_toc.next_question_id ,
	survey_questions_toc.branches ,
	survey_questions_toc.branch_next_question_id
FROM
	survey_questions_toc
JOIN surveys_toc ON survey_questions_toc.survey_id = surveys_toc.survey_id
JOIN questions_toc ON survey_questions_toc.question_id = questions_toc.question_id
WHERE
	survey_questions_toc.survey_id = ?
AND survey_questions_toc.question_id = ?");

	mysqli_stmt_bind_param($stmt, 'ii', $survey_id, $question_id);
    mysqli_stmt_execute($stmt);

      mysqli_stmt_bind_result($stmt, $row->survey_title, $row->question_description, $row->question_id, $row->next_question_id, $row->branches, $row->branch_next_question_id);


while (mysqli_stmt_fetch($stmt)) {
	$survey_title = $row->survey_title;
	$question_description = $row->question_description;
	if ($row->branches == 'n') {
		$question_id = (int) $row->next_question_id;
		$next_link = '<a href="survey_json_test.php?surveyID='.$survey_id.'&questionID='.$question_id.'">Next</a>';
	} else {
		$question_id = (int) $row->next_question_id;
		$branch_question_id = (int) $row->branch_next_question_id;
		$next_link = '<a href="survey_json_test.php?surveyID='.$survey_id.'&questionID='.$question_id.'">Next Yes</a> / <a href="survey_json_test.php?surveyID='.$survey_id.'&questionID='.$branch_question_id.'">Next No</a>';
	}
}

?>


<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?php echo $survey_title;?></title>
</head>

<body>
<pre>
<?php echo $question_description;?>
</pre>
<?php echo $next_link;?>
</body>
</html>