<?php

$mysqli = new mysqli('easyfeedback.clu994zhhlsg.ap-southeast-2.rds.amazonaws.com:3306', 'easyfeedback', 'Chianti2017', 'easyFEEDBACK');

/*
 * This is the "official" OO way to do it,
 * BUT $connect_error was broken until PHP 5.2.9 and 5.3.0.
 */
if ($mysqli->connect_error) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
            . $mysqli->connect_error);
}

/*
 * Use this instead of $connect_error if you need to ensure
 * compatibility with PHP versions prior to 5.2.9 and 5.3.0.
 */
if (mysqli_connect_error()) {
    die('Connect Error (' . mysqli_connect_errno() . ') '
            . mysqli_connect_error());
}

if (!isset($_SESSION)) {
	session_start();
}

date_default_timezone_set('Australia/Melbourne');
$now_date_time = date('Y-m-d H:i:s');


$question_id = $_POST['q'];
$question_answer = $_POST['a'];
$question_text = $_POST['t'];
$campaign_id = $_POST['c'];
$recipient_id = $_POST['r'];

$_SESSION['survey_responses'][$question_id] = $question_answer;

$message_array = array();

$delete_stmt = mysqli_prepare($mysqli, "delete from responses_toc where campaign_id = ? and recipient_id = ? and survey_question_id = ?");
mysqli_stmt_bind_param($delete_stmt, 'iii', $campaign_id, $recipient_id, $question_id);
mysqli_stmt_execute($delete_stmt);

if((string)(int)$question_answer === $question_answer) {
	$stmt = mysqli_prepare($mysqli, "insert into responses_toc (campaign_id, recipient_id, survey_question_id, survey_question_text, survey_response_numeric, survey_response_date) values (?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($stmt, 'iiisis', $campaign_id, $recipient_id, $question_id, $question_text, $question_answer, $now_date_time);
} else {
	$stmt = mysqli_prepare($mysqli, "insert into responses_toc (campaign_id, recipient_id, survey_question_id, survey_question_text, survey_response_text, survey_response_date) values (?, ?, ?, ?, ?, ?)");
	mysqli_stmt_bind_param($stmt, 'iiisss', $campaign_id, $recipient_id, $question_id, $question_text, $question_answer, $now_date_time);
}


   if (mysqli_stmt_execute($stmt)) {

   			$message_array[] = array("heading"=>"Response Recorded.", "message"=>"The response has been recorded for recipient $recipient_id for campaign $campaign_id.");
			
		} else {

			$message_array[] = array("heading"=>"Response Not Recorded.", "message"=>"The response has not been recorded.\n\nPlease try again.");

		}

	  mysqli_stmt_free_result($delete_stmt);
	  mysqli_close($delete_stmt);
      mysqli_stmt_free_result($stmt);
	  mysqli_close($stmt);
	  mysqli_close($mysqli);

echo json_encode($message_array);

?>
